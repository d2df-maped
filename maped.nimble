# Package

version       = "0.1.0"
author        = "fgsfds"
description   = "Doom 2D Forever Map Editor"
license       = "GPL-3.0-only"
srcDir        = "src"
bin           = @["maped"]
backend       = "cpp"

# Dependencies

requires "nim >= 1.4.6"
requires "sdl2 >= 2.0.4"
requires "opengl >= 1.2.2"
requires "zippy >= 0.7.3"
