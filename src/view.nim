import
  utils, gfx, gui, map, config

var
  viewPos*: DFPoint = (0, 0)
  viewSize*: DFSize = (winDefaultW, winDefaultH)

var
  viewHsc: CustomScrollBar
  viewVsc: CustomScrollBar

const
  bboxColor = initDFColor(0xFF, 0xFF, 0xFF, 0xE0)
  selectColor = initDFColor(0xFF, 0x00, 0x00, 0xE0)
  dotColor = initDFColor(0x00, 0x00, 0xFF, 0xFF)

method render*(self: MapObject) {.base.} = discard

method render*(self: MapPanel) =
  if intersect(self.position, self.size, viewPos, viewSize):
    if self.texture != nil:
      gfx.drawQuad(self.position, self.size, self.texture.tex, (0xFF'u8, 0xFF'u8, 0xFF'u8, 0xFF'u8 - self.alpha))
    else:
      gfx.drawQuad(self.position, self.size, (0x00'u8, 0x00'u8, 0x00'u8, 0x80'u8))

method render*(self: MapItem) =
  if self.tex != nil:
    if intersect(self.position, self.size, viewPos, viewSize):
      gfx.drawQuad(self.position, self.tex.size, self.tex)
      gfx.drawRect(self.position, self.size, bboxColor)
      gfx.drawPoint(self.position, dotColor, 2)

method render*(self: MapArea) =
  if self.tex != nil:
    if intersect(self.position, self.size, viewPos, viewSize):
      gfx.drawQuad(self.position - self.center, self.tex.size, self.tex)
      gfx.drawRect(self.position, self.size, bboxColor)
      gfx.drawPoint(self.position, dotColor, 2)

method render*(self: MapMonster) =
  if self.tex != nil:
    if intersect(self.position, self.size, viewPos, viewSize):
      if self.direction == dirLeft:
        var xx = self.center.x - self.delta.x + self.size.w
        xx = self.tex.size.w - xx - self.center.x
        xx = self.position.x - self.center.x - xx
        let yy = self.position.y - self.center.y + self.delta.y
        gfx.drawQuad((xx, yy), self.tex.size, self.tex, flipX = true)
      else:
        gfx.drawQuad(self.position - self.center + self.delta, self.tex.size, self.tex)
      gfx.drawRect(self.position, self.size, bboxColor)
      gfx.drawPoint(self.position, dotColor, 2)

method render*(self: MapTrigger) =
  discard

proc gridPoints(): seq[DFPoint] =
  let startPos = (viewPos div gridSize) * gridSize
  let endPos = startPos + viewSize
  result = @[]
  for y in countup(startPos.y, endPos.y + gridSize - 1, gridSize):
    for x in countup(startPos.x, endPos.x + gridSize - 1, gridSize):
      result.add((x, y))

proc renderGrid() =
  gfx.drawPoints(gridPoints(), colWhite)

template renderPanels(self: Map, kinds: uint16): untyped =
  for x in self.panels[]:
    if (x.kind.uint16 and kinds) != 0:
      x.render()

proc render(self: Map) =
  if config.showLayer[lrBackground]:
    self.renderPanels(panBack.uint16)

  if config.showLayer[lrLiquids]:
    var mask = panLiftUp.uint16 or panLiftDown.uint16 or panLiftRight.uint16 or panLiftLeft.uint16
    mask = mask or panOpenDoor.uint16 or panClosedDoor.uint16 or panBlockMon.uint16 # TODO: disable when preview mode
    self.renderPanels(mask)

  if config.showLayer[lrWalls]:
    self.renderPanels(panWall.uint16)

  if config.showLayer[lrSteps]:
    self.renderPanels(panStep.uint16)

  if config.showLayer[lrItems]:
    for x in self.items[]: x.render()

  if config.showLayer[lrMonsters]:
    for x in self.monsters[]: x.render()

  if config.showLayer[lrAreas]:
    for x in self.areas[]: x.render()

  if config.showLayer[lrLiquids]:
    self.renderPanels(panWater.uint16 or panAcid1.uint16 or panAcid2.uint16)

  if config.showLayer[lrForeground]:
    self.renderPanels(panFore.uint16)

  if config.showLayer[lrTriggers]:
    for x in self.triggers[]: x.render()

proc render*(selectedList: seq[MapObject]) =
  let viewScreenPos = gui.viewportPos()
  let scrollSize: DFSize = (customBarWidth, customBarWidth)
  viewSize = gui.maxViewportSize()
  gfx.setViewport(viewScreenPos, viewSize + scrollSize)

  gfx.clear(config.bgColor)

  gfx.setOffset(viewPos)

  if currentMap != nil:
    currentMap.render()

  if config.showGrid:
    renderGrid()

  gfx.setOffset((0, 0))

  let mapSize = (if currentMap == nil: viewSize else: currentMap.header.size)
  viewHsc.hscroll(viewPos.x, viewSize.x, mapSize.x, viewSize.y)
  viewVsc.vscroll(viewPos.y, viewSize.y, mapSize.y, viewSize.x)
