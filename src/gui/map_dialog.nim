import
  std/strutils,
  ../thirdparty/imgui,
  message_box,
  ../utils, ../gfx, ../vfs, ../serialization

type
  MapDialog* = object
    title: string
    archive: FsArchive
    create: bool
    isOpen: bool
    listing: seq[string]
    error: bool
    mapName: string
    mapIndex: int
    overwriteBox: MessageBox
    errorBox: MessageBox

var
  defaultMapDialog*: MapDialog

const
  minSize = ImVec2(x: 200f, y: 250f)

proc listMaps*(self: var MapDialog): bool =
  self.mapIndex = -1
  self.listing.setLen(0)
  for f in self.archive.resources():
    if '/' notin f.path:
      let idx = self.listing.len()
      self.listing.add(f.path)
      if self.mapName == f.path:
        self.mapIndex = idx
  result = true

proc open*(self: var MapDialog, title: string, ar: FsArchive, create: bool, defaultName: string = "MAP01"): bool {.discardable.} =
  if self.isOpen: return false
  self.title = title
  self.archive = ar
  self.create = create
  self.error = false
  self.listing = @[]
  self.mapName = (if create: defaultName else: "")
  result = self.listMaps()
  self.isOpen = result

proc confirmSelection(self: var MapDialog) =
  if self.mapName != "":
    if self.create and self.mapName in self.listing:
      self.overwriteBox.open(mbQuestion, "Overwrite " & self.mapName & "?")
    else:
      self.isOpen = false

proc renderWindow(self: var MapDialog) =
  if imgui.inputText("##MapInput", self.mapName, 256):
    if self.mapName != "" and (self.create or self.mapName in self.listing):
      self.mapIndex = self.listing.find(self.mapName)
      self.confirmSelection()
    else:
      self.mapIndex = -1

  imgui.separator()

  if imgui.beginListBox("##MapList"):
    for i, x in self.listing:
      if imgui.selectable(x, (i == self.mapIndex), ImGuiSelectableFlags.AllowDoubleClick):
        self.mapIndex = i
        self.mapName = x
        if imgui.isMouseDoubleClicked(ImGuiMouseButton.Left):
          self.confirmSelection()
    imgui.endListBox()

  imgui.separator()

  if self.mapIndex < 0:
    imgui.pushInactive()

  if imgui.button("OK"):
    self.confirmSelection()

  if self.mapIndex < 0:
    imgui.popInactive()

  imgui.sameLine()

  if imgui.button("Cancel"):
    self.mapName = ""
    self.mapIndex = -1
    self.isOpen = false

proc processPopups(self: var MapDialog) =
  if self.overwriteBox.display():
    if self.overwriteBox.getResult() == 1:
      self.isOpen = false
  discard self.errorBox.display()

proc display*(self: var MapDialog): bool =
  result = false
  if not self.isOpen: return

  let maxSize = imgui.getIO().displaySize

  if not imgui.isPopupOpen(self.title):
    imgui.openPopup(self.title)

  imgui.setNextWindowSizeConstraints(minSize, maxSize)
  imgui.setNextWindowPos(maxSize * 0.5f, ImGuiCond.Appearing, ImVec2(x: 0.5f, y: 0.5f))
  imgui.setNextWindowSize(minSize, ImGuiCond.Appearing)

  if imgui.beginPopupModal(self.title, nil, ImGuiWindowFlags.NoScrollbar):
    self.renderWindow()
    self.processPopups()
    result = not self.isOpen
    if result: imgui.closeCurrentPopup()
    imgui.endPopup()

proc getResult*(self: MapDialog): string = 
  if self.error: "" else: self.mapName.toUpperAscii()

proc isReading*(self: MapDialog): bool =
  not self.create
