import
  std/macros, std/sets, std/enumutils,
  ../thirdparty/imgui,
  ../serialization, ../utils, ../gfx

proc emitEditControls*[T: enum](x: var set[T], label: string): bool =
  result = false
  imgui.doTreeNode(label):
    for e in T:
      var isOn = (e in x)
      var pressed = imgui.checkbox($e, isOn)
      if pressed:
        if isOn:
          x.incl(e)
        else:
          x.excl(e)
        result = true # something changed

proc emitEditControls*[T: enum](x: var T, label: string): bool =
  result = false
  imgui.doCombo(label, $x):
    for e in T.items():
      let isSelected = (x == e)
      if imgui.selectable($e, isSelected):
        x = e
        result = true # something changed
      if isSelected:
        imgui.setItemDefaultFocus()

proc emitEditControls*[T: SomeInteger](x: var T, label: string): bool =
  result = false
  var value: int32 = x.int32
  if imgui.inputInt(label, value, 1, 10):
    x = cast[T](value)
    result = true

proc emitEditControls*(x: var bool, label: string): bool =
  result = imgui.checkbox(label, x)

proc emitEditControls*(v: var DFPoint, label: string): bool =
  result = false
  var value: array[2, int32] = [ v.x.int32, v.y.int32 ]
  if imgui.inputInt2(label, value):
    v.x = value[0].int
    v.y = value[1].int
    result = true

proc emitEditControls*(v: var DFSize, label: string): bool =
  result = false
  var value: array[2, int32] = [ v.w.int32, v.h.int32 ]
  if imgui.inputInt2(label, value):
    v.w = value[0].int
    v.h = value[1].int
    result = true

proc emitEditControls*(v: var string, strSize: int, label: string): bool =
  result = imgui.inputText(label, v, strSize)

proc emitEditControlsForRef*[T: Serializable](v: var T, label: string): bool =
  result = false
  # display the button to change reference
  if imgui.button("..."):
    return true
  # display the name of the referenced object
  imgui.sameLine()
  imgui.doInactive():
    var valStr = (if v == nil: "null" else: v.name)
    discard imgui.inputText(label, valStr, valStr.len() + 1)

proc emitEditControls*[T: Serializable](self: T, objLabel: string): bool =
  result = false
  for origFieldName, fieldSym in fieldPairs(self[]):
    when not fieldSym.hasCustomPragma(invisible):
      # rename field if needed
      when fieldSym.hasCustomPragma(serializedFieldName):
        const fieldName = fieldSym.getCustomPragmaVal(serializedFieldName)
      elif origFieldName == "kind":
        const fieldName = "type" # HACK: `type` is reserved, so instead of spamming pragmas everywhere we do this
      else:
        const fieldName = origFieldName.snakeCase()
      # spawn control for this field
      when fieldSym.hasCustomPragma(nonEditable):
        imgui.pushInactive() # make this control inactive
      var label = fieldName
      when fieldSym is Serializable:
        discard # by default we don't emit references or substructs to allow for custom handling
      elif fieldSym is string:
        when fieldSym.hasCustomPragma(serialization.maxSize):
          const strSize = fieldSym.getCustomPragmaVal(serialization.maxSize)
        else:
          const strSize = 256 # FIXME
        if fieldSym.emitEditControls(strSize, label):
          result = true
      else:
        if fieldSym.emitEditControls(label):
          result = true
      # next control should be active
      when fieldSym.hasCustomPragma(nonEditable):
        imgui.popInactive()
