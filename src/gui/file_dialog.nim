import
  std/os, std/sets, std/strutils, std/math, std/algorithm,
  ../thirdparty/imgui,
  message_box,
  ../utils, ../gfx

type
  FileDialog* = object
    title: string
    rootPath: string
    curPath: string
    filter: HashSet[string]
    create: bool
    isOpen: bool
    listing: seq[string]
    error: bool
    showHidden: bool
    selectedPath: string
    selectedName: string
    selectedIdx: int
    overwriteBox: MessageBox
    errorBox: MessageBox

var
  defaultFileDialog*: FileDialog

const
  minSize = ImVec2(x: 600f, y: 400f)
  colWidth = 280f

# this is partially based on https://github.com/gallickgunner/ImGui-Addons (MIT license)

when doslikeFileSystem:
  proc listDrives(self: var FileDialog) =
    # HACK: this is slow and bad, but I don't think Nim's winlean module exposes GetLogicalDriveStrings
    for letter in 'A'..'Z':
      let drivePath = $letter & ":/"
      if dirExists(drivePath):
        self.listing.add(drivePath)

proc listDir(self: var FileDialog): bool =
  self.selectedIdx = -1
  self.selectedName = ""
  self.listing.setLen(0)
  result = false
  if self.curPath == "":
    result = true
    when doslikeFileSystem:
      self.listDrives()
  else:
    if self.curPath != "/":
      self.listing.add("..")
    result = true
    for kind, path in walkDir(self.curPath, true, true):
      if self.showHidden or not isHidden(self.curPath & "/" & path):
        if kind == pcDir or kind == pcLinkToDir:
          self.listing.add(path & "/")
        else:
          let (_, _, ext) = path.splitFile()
          if self.filter.len() == 0 or ext.toLowerAscii() in self.filter:
            self.listing.add(path)
  # sort them (folders first, then alphabetically)
  sort(self.listing, filenameCmp)

proc changeDir(self: var FileDialog, dir: string): bool =
  if dir == "..":
    if self.rootPath != "" and self.curPath == self.rootPath:
      return false # don't go above the root path
    if self.curPath == "" or self.curPath == "/":
      return false # can't go any higher
    if doslikeFileSystem and self.curPath[1 .. ^1] == ":":
      self.curPath = "" # we're at a drive root, go to the drive list
    else:
      self.curPath = self.curPath.parentDir().normalizeDFPath()
  elif dir != "":
    if (":/" notin dir) and dir[0] != '/':
      # relative to current dir
      let dirName = (if dir.endsWith('/'): dir[0 .. ^2] else: dir)
      self.curPath = self.curPath & "/" & dirName
    elif dir.startsWith(self.rootPath):
      # absolute path that's inside root path
      let dirPath = (if dir.endsWith('/'): dir[0 .. ^2] else: dir)
      self.curPath = dirPath
    else:
      return false
  else:
    when doslikeFileSystem:
      if self.rootPath == "":
        self.curPath = dir
  result = self.listDir()

proc open*(self: var FileDialog, title: string, path: string, filter: openArray[string], create: bool = false, rootPath: string = path): bool {.discardable.} =
  if self.isOpen: return false
  self.title = title
  self.curPath = (if path == "": path else: path.expandFilename().normalizeDFPath())
  when not doslikeFileSystem:
    if self.curPath == "": self.curPath = "/"
  if not dirExists(self.curPath):
    return false
  if rootPath == path:
    self.rootPath = self.curPath
  else:
    let tmpPath = (if rootPath == "": rootPath else: rootPath.expandFilename().normalizeDFPath())
    if self.curPath.startsWith(tmpPath):
      self.rootPath = tmpPath
    else:
      self.rootPath = self.curPath
  self.filter = filter.toHashSet()
  self.create = create
  self.listing = @[]
  self.error = false
  self.selectedPath = ""
  self.showHidden = false
  result = self.listDir()
  self.isOpen = result

proc confirmSelection(self: var FileDialog): bool {.discardable.} =
  result = true
  when doslikeFileSystem:
    if self.selectedName.len() == 2 and self.selectedName.endsWith(':'):
      self.selectedName &= '/'
  let isDir = self.selectedName.endsWith('/') or self.selectedName == ".."
  if isDir:
    result = self.changeDir(self.selectedName):
  else:
    let tmp = self.curPath & "/" & self.selectedName
    if self.create or fileExists(tmp):
      if self.create and fileExists(tmp):
        self.overwriteBox.open(mbQuestion, "Overwrite " & self.selectedName & "?")
      else:
        self.selectedPath = tmp
        self.isOpen = false
    else:
      self.error = true
      result = false
      self.isOpen = false

proc renderNavBar(self: var FileDialog) =
  let
    frameHeight = imgui.getFrameHeight()
    style = imgui.getStyle()
    nwSize = ImVec2(x: 0f, y: style.windowPadding.y * 2f + frameHeight)

  var dirList = self.curPath.split("/")
  var beginIndex = 0
  if doslikeFileSystem and (dirList.len() > 1 or dirList[0] != ""):
    dirList.insert("")
    beginIndex = 1

  imgui.beginChild("##NavWindow", nwSize, true, ImGuiWindowFlags(ImGuiWindowFlags.AlwaysAutoResize.int or ImGuiWindowFlags.NoScrollbar.int))

  for i, x in dirList:
    when doslikeFileSystem:
      let buttonName = (if x == "": "/" else: x)
    else:
      let buttonName = x
    # button for each directory
    if imgui.button(buttonName):
      if i != dirList.high:
        var dirName = dirList[beginIndex .. i].join("/")
        if dirName != "": dirName &= '/'
        discard self.changeDir(dirName)
    # little arrows in between
    if i != dirList.high:
      imgui.sameLine(0f, 0f)
      imgui.pushStyleColor(ImGuiCol.Button, ImVec4(x: 1f, y: 1f, z: 1f, w: 0.05f))
      discard imgui.arrowButtonEx("##Right", ImGuiDir.Right, ImVec2(x: frameHeight, y: frameHeight), ImGuiButtonFlags(ImGuiItemFlags.Disabled.int))
      imgui.sameLine(0f, 0f)
      imgui.popStyleColor()

  imgui.endChild()

proc keyboardControls(self: var FileDialog, colItemsLimit: int): bool =
  # keyboard navigation doesn't do shit because we're not using standard listboxes, so here goes
  result = false
  let oldIdx = self.selectedIdx
  if imgui.isKeyPressed(ImGuiKey.DownArrow) and self.selectedIdx < self.listing.len() - 1:
    self.selectedIdx.inc()
  elif imgui.isKeyPressed(ImGuiKey.UpArrow) and self.selectedIdx > 0:
    self.selectedIdx.dec()
  elif imgui.isKeyPressed(ImGuiKey.LeftArrow):
    self.selectedIdx = max(self.selectedIdx - colItemsLimit, 0)
  elif imgui.isKeyPressed(ImGuiKey.RightArrow):
    self.selectedIdx = min(self.selectedIdx + colItemsLimit, self.listing.high)
  if oldIdx != self.selectedIdx:
    self.selectedName = self.listing[self.selectedIdx]
    result = true
  if self.selectedIdx >= 0 and imgui.isKeyPressed(ImGuiKey.Enter, false):
    self.confirmSelection()

proc renderFileList(self: var FileDialog) =
  let 
    style = imgui.getStyle()
    pwSize = imgui.getWindowSize()
    listItemH = imguiCtx.fontSize + style.itemSpacing.y
    inputBarY = pwSize.y - imgui.getFrameHeightWithSpacing() * 2.5f - style.windowPadding.y
    windowH = inputBarY - imgui.getCursorPosY() - style.itemSpacing.y
    windowContentH = windowH - style.windowPadding.y * 2f
    minContentSize = pwSize.x - style.windowPadding.x * 4f

  if windowContentH <= 0f:
    return

  var colItemsLimit = max(1f, windowContentH / listItemH).int
  var numCols = max(1f, ceil(self.listing.len().float32 / colItemsLimit.float32)).int
  if numCols > 64:
    let exceed = (numCols - 64) * colItemsLimit
    colItemsLimit += ceil(exceed.float32 / 64f).int
    numCols = max(1f, ceil(self.listing.len().float32 / colItemsLimit.float32)).int

  var contentW = numCols.float32 * colWidth
  if contentW < minContentSize:
    contentW = 0f

  imgui.setNextWindowContentSize(ImVec2(x: contentW, y: 0f))
  imgui.beginChild("##ScrollRegion", ImVec2(x: 0f, y: windowH), true, ImGuiWindowFlags.HorizontalScrollbar)
  imgui.columns(numCols)

  var list = self.listing
  var keysUsed = self.keyboardControls(colItemsLimit)
  var item = 0
  for i, x in list:
    item.inc()
    let isDir = (x == ".." or x.endsWith('/'))
    if isDir: imgui.pushStyleColor(ImGuiCol.Text, ImVec4(x: 0.7f, y: 0.7f, z: 0.7f, w: 1f))
    let selected = (self.selectedIdx == i)
    if selected and keysUsed:
      imgui.setScrollHereX(0f)
    if imgui.selectable(x, selected, ImGuiSelectableFlags.AllowDoubleClick):
      self.selectedIdx = i
      self.selectedName = x
      if imgui.isMouseDoubleClicked(ImGuiMouseButton.Left):
        self.confirmSelection()
    if (item mod colItemsLimit) == 0:
      imgui.nextColumn()
    if isDir: imgui.popStyleColor()

  imgui.columns(1)
  imgui.endChild()

proc renderInputRegion(self: var FileDialog) =
  let style = imgui.getStyle()
  let buttonSize = imgui.calcTextSize("Create File").x
  imgui.beginChild("##InputRegion", ImVec2(x: 0f, y: 0f), true)

  imgui.pushItemWidth((buttonSize + style.itemSpacing.x) * -2f)
  var tmp = self.selectedName
  if imgui.inputText("##FileNameInput", tmp, 256, ImGuiInputTextFlags.EnterReturnsTrue):
    if tmp != "":
      self.selectedName = tmp
      self.confirmSelection()
  imgui.popItemWidth()

  imgui.sameLine(imgui.getWindowSize().x - buttonSize * 2 - style.windowPadding.x - style.itemSpacing.x)
  if imgui.button((if self.create: "Save" else: "Open"), ImVec2(x: buttonSize, y: 0f)):
    self.confirmSelection()

  imgui.sameLine()
  if imgui.button("Cancel", ImVec2(x: buttonSize, y: 0f)):
    self.selectedName = ""
    self.selectedPath = ""
    self.isOpen = false

  if imgui.checkbox("Show hidden files", self.showHidden):
    discard self.listDir()

  var extText: string
  if self.filter.len() > 0:
    for x in self.filter:
      extText.addSep()
      extText &= x
  else:
    extText = "all files"

  let textSize = imgui.calcTextSize(extText).x
  imgui.sameLine(imgui.getWindowSize().x - style.windowPadding.x - textSize)
  imgui.pushStyleColor(ImGuiCol.Text, ImVec4(x: 0.6f, y: 0.6f, z: 0.6f, w: 1f))
  imgui.text(extText)
  imgui.popStyleColor()

  imgui.endChild()

proc processPopups(self: var FileDialog) =
  if self.overwriteBox.display():
    if self.overwriteBox.getResult() == 1:
      self.selectedPath = self.curPath & "/" & self.selectedName
      self.isOpen = false
  discard self.errorBox.display()

proc display*(self: var FileDialog): bool =
  result = false
  if not self.isOpen: return

  let maxSize = imgui.getIO().displaySize

  if not imgui.isPopupOpen(self.title):
    imgui.openPopup(self.title)

  imgui.setNextWindowSizeConstraints(minSize, maxSize)
  imgui.setNextWindowPos(maxSize * 0.5f, ImGuiCond.Appearing, ImVec2(x: 0.5f, y: 0.5f))
  imgui.setNextWindowSize(minSize, ImGuiCond.Appearing)

  if imgui.beginPopupModal(self.title, nil, ImGuiWindowFlags.NoScrollbar):
    self.renderNavBar()
    self.renderFileList()
    self.renderInputRegion()
    self.processPopups()
    result = not self.isOpen
    if result: imgui.closeCurrentPopup()
    imgui.endPopup()

proc getResultPath*(self: FileDialog): string =
  if self.error: "" else: self.selectedPath

proc getResultName*(self: FileDialog): string =
  if self.error: "" else: self.selectedName

proc isReading*(self: FileDialog): bool =
  not self.create
