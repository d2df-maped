import
  std/os, std/strutils,
  ../thirdparty/imgui,
  ../utils, ../gfx

type
  MessageBoxKind* = enum
    mbInfo
    mbWarning
    mbError
    mbFatalError
    mbQuestion

  MessageBox* = object
    kind: MessageBoxKind
    text: string
    selected: int # -1 => nothing selected, 0 => no, 1 => yes
    title: string
    isOpen: bool

var
  defaultInfoBox*: MessageBox
  defaultWarningBox*: MessageBox
  defaultErrorBox*: MessageBox
  defaultFatalBox*: MessageBox

proc open*(self: var MessageBox, kind: MessageBoxKind, text: string): bool {.discardable.} =
  if self.isOpen: return false
  self.kind = kind
  self.text = text
  self.title =
    case kind
    of mbInfo: "Information"
    of mbWarning: "Warning"
    of mbError: "Error"
    of mbFatalError: "Fatal Error"
    of mbQuestion: "Question"
  self.selected = -1
  self.isOpen = true
  result = true

proc display*(self: var MessageBox): bool =
  result = false
  if not self.isOpen: return
  let maxSize = imgui.getIO().displaySize

  if not imgui.isPopupOpen(self.title):
    imgui.openPopup(self.title)

  imgui.setNextWindowPos(maxSize * 0.5f, ImGuiCond.Appearing, ImVec2(x: 0.5f, y: 0.5f))
  imgui.setNextWindowSize(ImVec2(x: 0f, y: 0f))

  if imgui.beginPopupModal(self.title, nil, ImGuiWindowFlags.AlwaysAutoResize):
    imgui.textWrapped(self.text)
    imgui.separator()
    let bsize = ImVec2(x: 50f, y: 0f)
    if self.kind == mbQuestion:
      imgui.dummy(ImVec2(x: bsize.x, y: 1f))
      imgui.sameLine()
      if imgui.button("Yes", bsize):
        self.selected = 1
        result = true
      imgui.sameLine()
      if imgui.button("No", bsize):
        self.selected = 0
        result = true
      imgui.sameLine()
      imgui.dummy(ImVec2(x: bsize.x, y: 1f))
    else:
      imgui.dummy(ImVec2(x: bsize.x, y: 1f))
      imgui.sameLine()
      if imgui.button("OK", bsize):
        self.selected = 1
        result = true
      imgui.sameLine()
      imgui.dummy(ImVec2(x: bsize.x, y: 1f))
    if result:
      self.isOpen = false
      imgui.closeCurrentPopup()
    imgui.endPopup()

proc show*(self: var MessageBox, kind: MessageBoxKind, text: string): bool =
  if not self.isOpen:
    if not self.open(kind, text):
      return false
  result = self.display()

proc getResult*(self: var MessageBox): int =
  result = self.selected
  self.selected = -1
