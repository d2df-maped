import
  ../thirdparty/imgui,
  ../utils, ../gfx

proc toImVec2*(self: DFSize): ImVec2 =
  ImVec2(x: self.w.float32, y: self.h.float32)

proc toImVec4*(c: DFColor): ImVec4 =
  ImVec4(
    x: c.r.float32 / 255f,
    y: c.g.float32 / 255f,
    z: c.b.float32 / 255f,
    w: c.a.float32 / 255f
  )
