import
  std/os, std/sets, std/strutils, std/math, std/algorithm, std/streams,
  ../thirdparty/imgui,
  message_box, common,
  ../utils, ../gfx, ../vfs

type
  WadDialogKind* = enum
    wdAny
    wdTextures

  WadDialog* = object
    kind: WadDialogKind
    title: string
    wadDir: string
    wad: FsArchive
    wadIndex: int
    wadList: seq[string]
    wadName: string
    res: Stream
    resIndex: int
    resName: string
    resList: seq[string]
    resPath: string
    resFullPath: string
    resFile: FsFileEntry
    isOpen: bool
    error: bool
    tex: Texture
    checkerTex: Texture
    overwriteBox: MessageBox
    errorBox: MessageBox

var
  defaultWadDialog*: WadDialog

const
  minSize = ImVec2(x: 500f, y: 400f)
  colWidth = 200f

proc wadCmp(x, y: string): int =
  if x == "<special>": -1
  elif y == "<special>": 1
  elif x[0] == '<' and y[0] != '<': -1
  elif y[0] == '<' and x[0] != '<': 1
  else: system.cmp(x, y)

proc clearTextures(self: var WadDialog) =
  if self.checkerTex != nil:
    self.checkerTex.deinit()
    self.tex = nil
  if self.tex != nil:
    if self.tex.path notin waterTextureNames:
      self.tex.deinit()
    self.tex = nil

proc updatePreview(self: var WadDialog) =
  self.clearTextures()
  if self.wadName == "<special>":
    self.checkerTex = newCheckersTexture("$CHECKER", initDFSize(16, 16))
    self.tex = gfx.getTexture(self.resName)
  elif self.wad != nil:
    let localPath = self.resPath & self.resName
    var f = self.wad.readFile(localPath)
    if f != nil:
      if f.isAnimTexture():
        self.tex = newAnimTexture("$PREVIEW", f)
      else:
        let contents = f.readAll()
        self.tex = newTexture("$PREVIEW", contents)
      f.close()
      if self.tex != nil:
        self.checkerTex = newCheckersTexture("$CHECKER", self.tex.size)

proc listWadDir(self: var WadDialog): bool =
  if not dirExists(self.wadDir):
    return false
  result = true
  if self.kind == wdTextures:
    self.wadList.add("<special>")
  if mapArchive != nil:
    self.wadList.add("<map>")
  for kind, path in walkDir(self.wadDir, true, true):
    if kind == pcFile or kind == pcLinkToFile:
      let (_, _, ext) = path.splitFile()
      if ext.toLowerAscii() in archiveExts:
        self.wadList.add(path)
  # sort them (folders first, then alphabetically)
  sort(self.wadList, wadCmp)

proc listResDir(self: var WadDialog): bool =
  result = false
  self.resName = ""
  self.resIndex = -1
  self.resList = @[]
  if self.wad == nil:
    self.resList &= waterTextureNames
    result = true
  else:
    if self.resPath != "":
      self.resList.add("..")
    for f in self.wad.resources(self.resPath):
      var localName = f.path
      localName.removePrefix(self.resPath)
      let elements = localName.split('/', 1)
      if elements.len() == 1:
        self.resList.add(elements[0]) # file
      else:
        let dirName = elements[0] & '/'
        if dirName notin self.resList:
          self.resList.add(dirName) # dir
    result = true
    sort(self.resList, filenameCmp)

proc changeWad(self: var WadDialog, wadName: string, force: bool = false): bool =
  result = false
  var newWad: FsArchive = self.wad
  var newWadName = ""
  if wadName == "<map>" or wadName == "":
    newWad = mapArchive
  elif wadName == "<special>":
    newWad = nil
    newWadName = "<special>"
  else:
    let wadFile = self.wadDir & "/" & wadName
    if fileExists(wadFile):
      newWad = vfs.mount(wadFile, false, true)
      newWadName = wadName
  if force or self.wad != newWad:
    if self.wad != nil: vfs.unmount(self.wad)
    self.wad = newWad
    self.resPath = ""
    self.wadName = newWadName
    self.resIndex = -1
    self.resFullPath = ""
    self.resName = ""
    self.clearTextures()
    result = self.listResDir()

proc changeResDir(self: var WadDialog, dir: string): bool =
  if dir == "..":
    if self.resPath == "":
      return false
    self.resPath = self.resPath.parentDir().normalizeDFPath()
    if self.resPath == ".":
      self.resPath = ""
  elif dir.endsWith('/'):
    self.resPath.addSep("/")
    self.resPath &= dir
  else:
    return false
  result = self.listResDir()

proc open*(self: var WadDialog, kind: WadDialogKind, title: string, wadDir: string): bool {.discardable.} =
  result = false
  if self.isOpen: return
  self.kind = kind
  self.title = title
  self.wadDir = wadDir
  self.wad = nil
  self.wadIndex = 0
  self.wadName = ""
  self.wadList = @[]
  self.res = nil
  self.resIndex = -1
  self.resName = ""
  self.resPath = ""
  self.resFullPath = ""
  self.resFile = nil
  self.resList = @[]
  self.tex = nil
  self.checkerTex = nil
  result = self.listWadDir()
  result = result and self.changeWad("<special>", true)
  self.isOpen = result
  self.error = false

proc confirmSelection(self: var WadDialog): bool {.discardable.} =
  result = true
  let isDir = self.resName.endsWith('/') or self.resName == ".."
  if isDir:
    result = self.changeResDir(self.resName):
  else:
    if not vfs.resExists(self.resFullPath):
      self.error = true
      result = false
      self.isOpen = false

proc renderWadList(self: var WadDialog, windowSize: ImVec2) =
  imgui.beginChild("##WadRegion", windowSize, true)

  for i, x in self.wadList:
    let selected = (self.wadIndex == i)
    if imgui.selectable(x, selected):
      if self.changeWad(self.wadList[i]):
        self.wadIndex = i

  imgui.endChild()

proc keyboardControls(self: var WadDialog): bool =
  # keyboard navigation doesn't do shit because we're not using standard listboxes, so here goes
  result = false
  let oldIdx = self.resIndex
  if imgui.isKeyPressed(ImGuiKey.DownArrow) and self.resIndex < self.resList.len() - 1:
    self.resIndex.inc()
  elif imgui.isKeyPressed(ImGuiKey.UpArrow) and self.resIndex > 0:
    self.resIndex.dec()
  if oldIdx != self.resIndex:
    self.resName = self.resList[self.resIndex]
    result = true
  if self.resIndex >= 0 and imgui.isKeyPressed(ImGuiKey.Enter, false):
    self.confirmSelection()

proc renderResList(self: var WadDialog, windowSize: ImVec2) =
  imgui.beginChild("##ResRegion", windowSize, true)

  var list = self.resList
  let keysUsed = self.keyboardControls()
  var updateNeeded = keysUsed
  for i, x in list:
    let isDir = (x == ".." or x.endsWith('/'))
    if isDir: imgui.pushStyleColor(ImGuiCol.Text, ImVec4(x: 0.7f, y: 0.7f, z: 0.7f, w: 1f))
    let selected = (self.resIndex == i)
    if imgui.selectable(x, selected, ImGuiSelectableFlags.AllowDoubleClick):
      self.resIndex = i
      self.resName = x
      updateNeeded = true
      if imgui.isMouseDoubleClicked(ImGuiMouseButton.Left):
        self.confirmSelection()
    if isDir: imgui.popStyleColor()

  imgui.endChild()

  if updateNeeded:
    self.resFullPath = self.wadName & ":" & self.resPath & self.resName
    if self.kind == wdTextures:
      self.updatePreview()

proc renderPreviewArea(self: var WadDialog, windowSize: ImVec2) =
  imgui.beginChild("##PreviewRegion", windowSize, true, ImGuiWindowFlags.HorizontalScrollbar)

  if self.tex != nil and self.checkerTex != nil:
    let px = imgui.getCursorPosX()
    imgui.image(self.checkerTex.glTex.uint32, self.checkerTex.size.toImVec2())
    imgui.sameLine()
    imgui.setCursorPosX(px)
    imgui.image(self.tex.glTex.uint32, self.tex.size.toImVec2())
    imgui.separator()
    if self.tex.frames > 1:
      imgui.text("Animated texture")
      imgui.text("Frames", ": ", self.tex.frames)
    else:
      imgui.text("Texture")
    imgui.text("Width", ": ", self.tex.size.w)
    imgui.text("Height", ": ", self.tex.size.h)
  elif self.resIndex != -1 and self.resName != "":
    imgui.text(self.resName)
    imgui.separator()
    if self.resName == ".." or self.resName.endsWith("/"):
      imgui.text("Section")
    else:
      imgui.text("Resource")

  imgui.endChild()

proc renderInputRegion(self: var WadDialog) =
  let style = imgui.getStyle()
  let buttonSize = imgui.calcTextSize("Select File").x
  let numButtons = (if self.kind == wdTextures: 3f else: 2f)

  imgui.beginChild("##InputRegion", ImVec2(x: 0f, y: 0f), true)

  imgui.pushItemWidth((buttonSize + style.itemSpacing.x) * -numButtons)
  var tmp = self.resFullPath
  discard imgui.inputText("##ResNameInput", tmp, 256, ImGuiInputTextFlags.ReadOnly)
  imgui.popItemWidth()

  let offset =
    if self.kind == wdTextures:
      buttonSize * 3 + style.windowPadding.x + style.itemSpacing.x * 2f
    else:
      buttonSize * 2 + style.windowPadding.x + style.itemSpacing.x

  imgui.sameLine(imgui.getWindowSize().x - offset)

  if self.kind == wdTextures:
    if imgui.button("Add", ImVec2(x: buttonSize, y: 0f)):
      if self.confirmSelection():
        self.isOpen = true # keep open
        # TODO: add texture
    imgui.sameLine()

  if imgui.button("Select", ImVec2(x: buttonSize, y: 0f)):
    self.confirmSelection()

  imgui.sameLine()
  if imgui.button("Cancel", ImVec2(x: buttonSize, y: 0f)):
    self.resFullPath = ""
    self.isOpen = false

  imgui.endChild()

proc renderColumnsArea(self: var WadDialog) =
  let
    style = imgui.getStyle()
    pwSize = imgui.getWindowSize()
    listItemH = imguiCtx.fontSize + style.itemSpacing.y
    inputBarY = pwSize.y - imgui.getFrameHeightWithSpacing() * 1.5f - style.windowPadding.y
    windowH = inputBarY - imgui.getCursorPosY() - style.itemSpacing.y
    windowW = pwSize.x - style.windowPadding.x * 2f - style.itemSpacing.x * 2f
    windowContentH = windowH - style.windowPadding.y * 2f

  if windowContentH > 0f:
    # could've used columns() here, but their width is hard to get right
    self.renderWadList(ImVec2(x: windowW * 0.3f, y: windowH))
    imgui.sameLine()
    self.renderResList(ImVec2(x: windowW * 0.3f, y: windowH))
    imgui.sameLine()
    self.renderPreviewArea(ImVec2(x: windowW * 0.4f, y: windowH))

proc processPopups(self: var WadDialog) =
  if self.overwriteBox.display():
    if self.overwriteBox.getResult() == 1:
      self.isOpen = false
  discard self.errorBox.display()

proc display*(self: var WadDialog): bool =
  result = false
  if not self.isOpen: return

  let maxSize = imgui.getIO().displaySize

  if not imgui.isPopupOpen(self.title):
    imgui.openPopup(self.title)

  imgui.setNextWindowSizeConstraints(minSize, maxSize)
  imgui.setNextWindowPos(maxSize * 0.5f, ImGuiCond.Appearing, ImVec2(x: 0.5f, y: 0.5f))
  imgui.setNextWindowSize(minSize, ImGuiCond.Appearing)

  if imgui.beginPopupModal(self.title, nil, ImGuiWindowFlags.NoScrollbar):
    self.renderColumnsArea()
    self.renderInputRegion()
    self.processPopups()
    result = not self.isOpen
    if result: imgui.closeCurrentPopup()
    imgui.endPopup()
