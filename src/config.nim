import utils, map

var
  showLayer*: array[MapLayer, bool] = [ true, true, true, true, true, true, true, true, true ]
  showMapEdges*: bool = true
  showGrid*: bool = true
  showMiniMap*: bool = false
  showPreview*: bool = false
  gridSize*: int = 16
  gridColor*: DFColor = (0xFF'u8, 0xFF'u8, 0xFF'u8, 0xFF'u8)
  bgColor*: DFColor = (0x40'u8, 0x60'u8, 0x7F'u8, 0xFF'u8)
