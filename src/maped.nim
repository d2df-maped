import
  std/streams, std/tables,
  map, serialization, utils, gfx, gui, view, vfs

var fileLog = newFileLogger("maped.log")
var consoleLog = newConsoleLogger()
addHandler(fileLog)
addHandler(consoleLog)

vfs.init()

vfs.mount("data/editor.wad", false)
vfs.mount("data/game.wad", false)

gfx.init()
gui.init()

map.init()
map.create()

while gfx.beginFrame():
  view.render(gui.selection)
  if not gui.mainWindow():
    break
  gfx.endFrame()
