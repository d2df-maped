import
  std/os, std/streams, std/strutils, std/tables,
  vfs/[common, dfwad, zip],
  utils

export
  streams, common, dfwad, zip

const
  archiveExts* = [".zip", ".dfz", ".pk3", ".wad"]

var
  archives: seq[FsArchive] = @[]
  mapArchive*: FsArchive = nil
  mapDeps*: seq[FsArchive] = @[]

proc openArchive*(path: string, write: bool, forceExisting: bool = false): FsArchive =
  result = nil
  let existing = fileExists(path)
  let (dir, name, ext) = splitFile(path)
  let arcName = name & ext
  let mode = (if write: (if forceExisting: fmReadWriteExisting else: fmReadWrite) else: fmRead)
  var stream = newFileStream(path, mode)
  if stream != nil:
    if stream.checkIfWad():
      result = newFsWadArchive(arcName, path)
    elif stream.checkIfZip():
      result = newFsZipArchive(arcName, path)
    else:
      raise newException(FsFormatError, "Unknown archive format extension: " & ext)
    result.stream = stream
    result.writeable = write
    result.existing = existing
    result.open()
  else:
    raise newException(IOError, "Could not open " & path)

proc openArchive*(name: string, stream: Stream): FsArchive =
  result = nil
  if stream.checkIfWad():
    result = newFsWadArchive(name, "")
  elif stream.checkIfZip():
    result = newFsZipArchive(name, "")
  else:
    return
  result.stream = stream
  result.writeable = false
  result.existing = true
  result.open()

proc unmount*(self: FsArchive) =
  var idx = archives.find(self)
  if idx >= 0:
    archives.delete(idx)

  if self == mapArchive:
    mapArchive = nil
  else:
    idx = mapDeps.find(self)
    if idx >= 0:
      mapDeps.delete(idx)

  self.close()

proc unmountAll*() =
  while archives.len() > 0:
    var ar = archives.pop()
    ar.close()
  mapArchive = nil
  mapDeps.setLen(0)

proc unmountMapDeps*() =
  while mapDeps.len() > 0:
    var ar = mapDeps.pop()
    ar.close()
  mapDeps.setLen(0)

proc unmountMap*() =
  if mapArchive != nil:
    mapArchive.unmount()

proc findArchive*(arName: string): FsArchive =
  if arName == "":
    return mapArchive
  for i, ar in archives:
    if cmpIgnoreCase(ar.name, arName) == 0:
      return ar
  result = nil

proc mount*(path: string, write: bool, forceExisting: bool = false, isMapArchive: bool = false, isMapDep: bool = false): FsArchive {.discardable.} =
  if path != "":
    let (dir, name, ext) = splitFile(path)
    var ar = findArchive(name & ext)
    if ar != nil: return ar # don't mount it again

  if isMapArchive:
    unmountMap()

  result = openArchive(path, write, forceExisting)

  if isMapArchive:
    mapArchive = result

  if isMapDep:
    mapDeps.add(result)

  archives.add(result)

proc init*() =
  addExitProc(vfs.unmountAll)

proc splitResPath*(path: string): tuple[archive, path: string] =
  var toks = path.split(':')
  if toks.len() < 2:
    raise newException(ValueError, "Invalid resource path: " & path)
  if toks[1][0] == '/':
    result = (toks[0], toks[1][1 .. ^1].toUpperAscii())
  else:
    result = (toks[0], toks[1].toUpperAscii())

proc open*(path: string, write: bool): Stream =
  let (arName, localPath) = path.splitResPath()
  var ar = findArchive(arName)
  if ar != nil:
    if write:
      return ar.writeFile(localPath)
    else:
      return ar.readFile(localPath)
  elif arName != "":
    # can't find archive in vfs; try to mount one
    var ar: FsArchive
    if fileExists(arName):
      ar = vfs.mount(arName, write, not write, isMapDep = not write)
    elif fileExists("wads/" & arName):
      ar = vfs.mount("wads/" & arName, write, not write, isMapDep = not write)
    else:
      return nil
    # archive found, get the res
    if ar != nil:
      if write:
        return ar.writeFile(localPath)
      else:
        return ar.readFile(localPath)
    result = nil

proc resExists*(path: string): bool =
  let (arName, localPath) = path.splitResPath()
  var ar = findArchive(arName)
  if ar == nil:
    result = false
  else:
    result = (ar.findFile(localPath) != nil)

iterator resources*(self: FsArchive, prefix: string = ""): FsFileEntry =
  let upperPrefix = prefix.toUpperAscii()
  for fname, f in self.files:
    if fname.startsWith(upperPrefix):
      yield f

iterator resources*(prefix: string): FsFileEntry =
  let (arName, localPath) = prefix.splitResPath()
  var ar = findArchive(arName)
  if ar != nil:
    for fname, f in ar.files:
      if fname.startsWith(localPath):
        yield f
