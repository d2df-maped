import
  std/tables, std/sequtils, std/strutils,
  sdl2, opengl, thirdparty/imgui, thirdparty/stb_image,
  utils, vfs

export
  sdl2, opengl

const
  winDefaultX* = SDL_WINDOWPOS_CENTERED
  winDefaultY* = SDL_WINDOWPOS_CENTERED
  winDefaultW* = 1024
  winDefaultH* = 768
  winFlags = SDL_WINDOW_OPENGL or SDL_WINDOW_RESIZABLE or SDL_WINDOW_SHOWN

const
  waterTextureNames* = ["_water_0", "_water_1", "_water_2"]
  waterTextureColors*: array[3, DFColor] = [
    (r: 0'u8, g: 0'u8, b: 255'u8, a: 128'u8),
    (r: 0'u8, g: 255'u8, b: 0'u8, a: 128'u8),
    (r: 255'u8, g: 0'u8, b: 0'u8, a: 128'u8)
  ]
  checkerSize* = 8

const
  colBlack* = initDFColor(0x00, 0x00, 0x00, 0xFF)
  colWhite* = initDFColor(0xFF, 0xFF, 0xFF, 0xFF)
  colGrey* = initDFColor(200, 200, 200, 0xFF)
  colDkGrey* = initDFColor(100, 100, 100, 0xFF)
  colRed* = initDFColor(0xFF, 0x00, 0x00, 0xFF)
  colBlue* = initDFColor(0x00, 0x00, 0xFF, 0xFF)

type
  Texture* = ref object of RootObj
    path*: string
    glTex*: GLuint
    glType*: GLenum
    size*: DFSize
    frames*: int # this is used for informational purposes only

var
  sdlWindow: WindowPtr = nil
  glContext: GlContextPtr = nil
  textures: Table[string, Texture] = initTable[string, Texture]()

proc isTexture*(data: string): bool =
  var ix, iy, icomp: int
  result = stb_image.infoFromMemory(data, ix, iy, icomp)

proc isAnimTexture*(ar: FsArchive): bool =
  (ar.findFile("TEXT/ANIM") != nil)

proc isAnimTexture*(stream: Stream): bool =
  let pos = stream.getPosition()
  var ar = vfs.openArchive("animtex", stream)
  if ar != nil:
    result = ar.isAnimTexture()
    ar.close()
    stream.setPosition(pos)
  else:
    result = false

proc isAnimTexture*(data: string): bool =
  var stream = newStringStream(data)
  result = isAnimTexture(stream)
  stream.close()

proc newTexture(): Texture =
  result = new(Texture)
  result.path = ""
  result.glTex = 0
  result.frames = 1
  glGenTextures(1, result.glTex.addr)
  glBindTexture(GL_TEXTURE_2D, result.glTex)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
  glBindTexture(GL_TEXTURE_2D, 0)

proc newFillTexture(name: string, size: DFSize, color: DFColor): Texture =
  result = newTexture()
  result.path = name
  result.size = size
  result.glType = GL_RGBA
  var buf = color.repeat(size.w * size.h)
  glBindTexture(GL_TEXTURE_2D, result.glTex)
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA.GLint, size.w.GLsizei, size.h.GLsizei, 0, GL_RGBA, GL_UNSIGNED_BYTE, buf[0].addr)
  glBindTexture(GL_TEXTURE_2D, 0)

proc newCheckersTexture*(name: string, size: DFSize, color1: DFColor = colWhite, color2: DFColor = colGrey): Texture =
  result = newTexture()
  result.path = name
  result.size = size
  result.glType = GL_RGB
  var buf = newSeq[tuple[r, g, b: uint8]](result.size.w * result.size.h)
  for i, c in buf.mpairs():
    let x = i mod result.size.w
    let y = i div result.size.w
    let rx = x mod (checkerSize * 2)
    let ry = y mod (checkerSize * 2)
    if (rx >= checkerSize) xor (ry >= checkerSize):
      c.r = color1.r
      c.g = color1.g
      c.b = color1.b
    else:
      c.r = color2.r
      c.g = color2.g
      c.b = color2.b
  glBindTexture(GL_TEXTURE_2D, result.glTex)
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB.GLint, result.size.w.GLsizei, result.size.h.GLsizei, 0, GL_RGB, GL_UNSIGNED_BYTE, buf[0].addr)
  glBindTexture(GL_TEXTURE_2D, 0)

proc newErrorTexture(name: string): Texture =
  result = newCheckersTexture(name, initDFSize(16, 16), colBlack, initDFColor(0xFF, 0x00, 0xFF, 0xFF))

proc newTexture*(name: string, contents: string, overrideSize: DFSize = initDFSize(0, 0)): Texture =
  var ix, iy, icomp: int
  if not stb_image.infoFromMemory(contents, ix, iy, icomp):
    error("Could not read texture: ", name, " ", stb_image.failureReason())
    return nil

  result = newTexture()
  result.path = name

  let wantComp = (if icomp == 4: 4 else: 3)
  var data = stb_image.loadFromMemory(contents, ix, iy, icomp, wantComp)
  var dataPtr: pointer = nil
  if (overrideSize.w > 0 and overrideSize.w < ix) or (overrideSize.h > 0 and overrideSize.h < iy):
    var newSize = overrideSize
    if newSize.w <= 0 or newSize.w > ix: newSize.w = ix
    if newSize.h <= 0 or newSize.h > iy: newSize.h = iy
    # cut out the top left part of the texture
    let dstStride = newSize.w * wantComp
    let srcStride = ix * wantComp
    var buf = newString(newSize.w * newSize.h * wantComp + 1)
    var src = 0
    var dst = 0
    for y in 0 ..< newSize.h:
      copyMem(buf[dst].addr, data[src].addr, dstStride)
      src += srcStride
      dst += dstStride
    result.size = newSize
    dataPtr = buf[0].addr
  else:
    result.size = initDFSize(ix, iy)
    dataPtr = data[0].addr

  result.glType = (if wantComp == 4: GL_RGBA else: GL_RGB)
  glBindTexture(GL_TEXTURE_2D, result.glTex)
  glTexImage2D(GL_TEXTURE_2D, 0, result.glType.GLint, result.size.w.GLsizei, result.size.h.GLsizei, 0, result.glType, GL_UNSIGNED_BYTE, dataPtr)
  glBindTexture(GL_TEXTURE_2D, 0)

proc newAnimTexture*(name: string, stream: Stream): Texture =
  result = nil

  var ar = vfs.openArchive(name, stream)
  if ar == nil:
    error("Could not load anim texture: ", name, " (file is not an archive)")
    return

  defer: ar.close()

  var f = ar.readFile("TEXT/ANIM")
  if f == nil:
    error("Could not load anim texture: ", name, " (file doesn't have ANIM)")
    return

  var info = f.readDFConfig()
  f.close()

  if not ("resource" in info and "framewidth" in info and "frameheight" in info):
    error("Could not load anim texture: ", name, " (file has invalid ANIM)")
    return
  
  let resName = "TEXTURES/" & info["resource"]
  f = ar.readFile(resName)
  if f == nil:
    error("Could not load anim texture: ", name, " (missing actual texture: ", resName, ")")
    return

  let contents = f.readAll()
  f.close()

  let overrideSize = initDFSize(info["framewidth"].parseInt(), info["frameheight"].parseInt())

  result = newTexture(name, contents, overrideSize)

  if result != nil and "framecount" in info:
    result.frames = info["framecount"].parseInt()

proc newTexture*(path: string, overrideSize: DFSize = (0, 0)): Texture =
  var f = vfs.open(path, false)
  if f == nil:
    error("Could not find texture: ", path)
    return nil
  if f.isAnimTexture():
    result = newAnimTexture(path, f)
  else:
    let contents = f.readAll()
    result = newTexture(path, contents, overrideSize)
  f.close()

proc deinit*(self: Texture) =
  if self.glTex != 0:
    glDeleteTextures(1, self.glTex.addr)
    self.glTex = 0

proc destroy*(self: Texture) =
  assert(self.path in textures, "orphaned texture: " & self.path)
  textures.del(self.path)
  self.deinit()

proc destroyTexture*(name: string): bool {.discardable.} =
  if name in textures:
    textures[name].destroy()
    result = true
  else:
    result = false

proc loadTexture*(name, contents: string): Texture {.discardable.} =
  result = newTexture(name, contents)
  if result != nil:
    if name in textures:
      textures[name].deinit()
    textures[name] = result

proc loadTexture*(path: string): Texture {.discardable.} =
  result = newTexture(path)
  if result != nil:
    if path in textures:
      textures[path].deinit()
    textures[path] = result

proc loadTextureExt*(path: string, name: string, overrideSize: DFSize = (0, 0)): Texture {.discardable.} =
  result = newTexture(path, overrideSize)
  if result != nil:
    result.path = name
    if name in textures:
      textures[name].deinit()
    textures[name] = result

proc getTexture*(name: string): Texture =
  if name in textures:
    return textures[name]
  result = loadTexture(name)

proc shutdown*() =
  imgui.shutdown()

  for tex in textures.mvalues():
    tex.deinit()
    tex = nil
  textures.clear()

  if sdlWindow != nil:
    if glContext != nil:
      discard sdlWindow.glMakeCurrent(nil)
      sdl2.glDeleteContext(glContext)
      glContext = nil
    sdlWindow.destroy()
    sdlWindow = nil
  sdl2.quit()

proc init*() =
  if not sdl2.init(INIT_VIDEO or INIT_EVENTS):
    error("Could not init SDL2: ", sdl2.getError())
    quit(-1)

  addExitProc(gfx.shutdown)

  sdlWindow = sdl2.createWindow("maped", winDefaultX, winDefaultY, winDefaultW, winDefaultH, winFlags)
  if sdlWindow == nil:
    error("Could not create SDL2 window: ", sdl2.getError())
    quit(-1)

  # the "opengl 2.1" backend of imgui actually only uses 1.x features
  discard sdl2.glSetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 1)
  discard sdl2.glSetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3)
  discard sdl2.glSetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY)
  glContext = sdlWindow.glCreateContext()
  if glContext == nil:
    error("Could not create SDL2 GL2.1 context: ", sdl2.getError())
    quit(-1)

  discard sdl2.glSetSwapInterval(1)

  opengl.loadExtensions()

  if not imgui.init(sdlWindow):
    error("Could not init ImGui")
    quit(-1)

  # spawn special textures
  textures["$$ERROR"] = newErrorTexture("$$ERROR")
  for i, x in waterTextureNames:
    textures[x] = newFillTexture(x, initDFSize(1, 1), waterTextureColors[i])

  glDisable(GL_DEPTH_TEST)
  glDisable(GL_ALPHA_TEST)
  glDisable(GL_CULL_FACE)
  glDisable(GL_STENCIL_TEST)
  glDisable(GL_LIGHTING)
  glDepthMask(GL_FALSE)

proc beginFrame*(): bool =
  result = true
  var event = sdl2.defaultEvent
  while sdl2.pollEvent(event):
    discard imgui.event(event)
    if event.kind == QuitEvent:
      return false

  glDisable(GL_SCISSOR_TEST)
  glClearColor(0, 0, 0, 0xFF)
  glClear(GL_COLOR_BUFFER_BIT)
  glEnable(GL_SCISSOR_TEST)

  imgui.beginFrame()

proc endFrame*() =
  imgui.endFrame()
  sdlWindow.glSwapWindow()

proc vidWidth*(): int = sdlWindow.getSize()[0]

proc vidHeight*(): int = sdlWindow.getSize()[1]

proc vidSize*(): DFSize =
  let s = sdlWindow.getSize()
  result = (s[0].int, s[1].int)

template withBlending(on: bool, actions: untyped): untyped =
  if on:
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
  actions
  if on:
    glDisable(GL_BLEND)

template withTexture(tex: GLuint, actions: untyped): untyped =
  if tex == 0:
    glDisable(GL_TEXTURE_2D)
  else:
    glEnable(GL_TEXTURE_2D)
    glBindTexture(GL_TEXTURE_2D, tex)
  actions
  glBindTexture(GL_TEXTURE_2D, 0.GLuint)

proc drawQuad*(pos: DFPoint, size: DFSize, tint: DFColor = colWhite) =
  let x0 = pos.x.GLint
  let y0 = pos.y.GLint
  let x1 = (pos.x + size.w).GLint
  let y1 = (pos.y + size.h).GLint
  glColor4ub(tint.r, tint.g, tint.b, tint.a)
  withTexture(0):
    withBlending(tint.a != 0xFF'u8):
      glBegin(GL_TRIANGLE_FAN)
      glVertex2i(x0, y0)
      glVertex2i(x1, y0)
      glVertex2i(x1, y1)
      glVertex2i(x0, y1)
      glEnd()

proc drawQuad*(pos: DFPoint, size: DFSize, tex: Texture, tint: DFColor = colWhite, flipX: bool = false) =
  let xn = ((if flipX: -size.w else: size.w) div tex.size.w).GLint
  let yn = (size.h div tex.size.h).GLint
  let x0 = pos.x.GLint
  let y0 = pos.y.GLint
  let x1 = (pos.x + size.w).GLint
  let y1 = (pos.y + size.h).GLint
  let blend = (tex.glType == GL_RGBA or tint.a != 0xFF'u8)
  glColor4ub(tint.r, tint.g, tint.b, tint.a)
  withTexture(tex.glTex):
    withBlending(blend):
      glBegin(GL_TRIANGLE_FAN)
      glTexCoord2i(0, 0)
      glVertex2i(x0, y0)
      glTexCoord2i(xn, 0)
      glVertex2i(x1, y0)
      glTexCoord2i(xn, yn)
      glVertex2i(x1, y1)
      glTexCoord2i(0, yn)
      glVertex2i(x0, y1)
      glEnd()

proc correctLine(x1, y1, x2, y2: var GLint) =
  # make lines only top-left/bottom-right and top-right/bottom-left
  if y2 < y1:
    swap(x1, x2)
    swap(y1, y2)
  # pixel perfect hack
  if x1 < x2:
    x2.inc()
  else:
    x1.inc()
  y2.inc()

proc drawRect*(pos: DFPoint, size: DFSize, tint: DFColor) =
  var x1 = pos.x.GLint
  var y1 = pos.y.GLint
  var x2 = (pos.x + size.w).GLint
  var y2 = (pos.y + size.h).GLint
  var nx1, ny1, nx2, ny2: GLint
  let blend = (tint.a != 0xFF'u8)
  if x1 > x2: swap(x1, x2)
  if y1 > y2: swap(y1, y2)
  glLineWidth(1f)
  glColor4ub(tint.r, tint.g, tint.b, tint.a)
  withTexture(0):
    withBlending(blend):
      glBegin(GL_LINES)
      (nx1, ny1, nx2, ny2) = (x1, y1, x2, y1)
      correctLine(nx1, ny1, nx2, ny2)
      glVertex2i(nx1, ny1)
      glVertex2i(nx2, ny2)
      (nx1, ny1, nx2, ny2) = (x2, y1, x2, y2)
      correctLine(nx1, ny1, nx2, ny2)
      glVertex2i(nx1, ny1)
      glVertex2i(nx2, ny2)
      (nx1, ny1, nx2, ny2) = (x2, y2, x1, y2)
      correctLine(nx1, ny1, nx2, ny2)
      glVertex2i(nx1, ny1)
      glVertex2i(nx2, ny2)
      (nx1, ny1, nx2, ny2) = (x1, y2, x1, y1)
      correctLine(nx1, ny1, nx2, ny2)
      glVertex2i(nx1, ny1)
      glVertex2i(nx2, ny2)
      glEnd()

proc drawPoints*(pts: openArray[DFPoint], tint: DFColor, size: int = 1) =
  glPointSize(size.GLfloat)
  glColor4ub(tint.r, tint.g, tint.b, tint.a)
  withTexture(0):
    withBlending(tint.a != 0xFF):
      glBegin(GL_POINTS)
      for pt in pts:
        glVertex2i(pt.x.GLint, pt.y.GLint)
      glEnd()

proc drawPoint*(pt: DFPoint, tint: DFColor, size: int = 1) =
  glPointSize(size.GLfloat)
  glColor4ub(tint.r, tint.g, tint.b, tint.a)
  withTexture(0):
    withBlending(tint.a != 0xFF):
      glBegin(GL_POINTS)
      glVertex2i(pt.x.GLint, pt.y.GLint)
      glEnd()

proc setViewport*(pos: DFPoint, size: DFSize) =
  let y = GLint(vidHeight() - pos.y - size.h)
  glScissor(pos.x.GLint, y, size.w.GLsizei, size.h.GLsizei)
  glViewport(pos.x.GLint, y, size.w.GLsizei, size.h.GLsizei)

  glMatrixMode(GL_PROJECTION)
  glLoadIdentity()
  glOrtho(0f, size.w.GLfloat, size.h.GLfloat, 0f, -1f, +1f)

  glMatrixMode(GL_MODELVIEW)
  glLoadIdentity()

proc setOffset*(pos: DFPoint) =
  glMatrixMode(GL_MODELVIEW)
  if pos.x == 0 and pos.y == 0:
    glLoadIdentity()
  else:
    glTranslatef(-pos.x.GLfloat, -pos.y.GLfloat, 0f)

proc clear*(c: DFColor) =
  glClearColor(c.r.GLfloat / 255f, c.g.GLfloat / 255f, c.b.GLfloat / 255f, 1f)
  glClear(GL_COLOR_BUFFER_BIT)
