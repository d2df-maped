import
  sdl2,
  base

{.warning[HoleEnumConv]: off.}

const
  platformBackendName = "imgui_impl_sdl"

var
  window: WindowPtr = nil
  lastClipboardText: cstring = nil # SDL wants the caller to free this shit
  mousePressed: array[3, bool] # left, right, middle
  ticks: uint64 = 0

proc igSDL2GetClipboardText*(userData: pointer): cstring {.cdecl.} =
  if lastClipboardText != nil:
    sdl2.freeClipboardText(lastClipboardText)
  lastClipboardText = sdl2.getClipboardText()
  result = lastClipboardText

proc igSDL2SetClipboardText*(userData: pointer, text: cstring) {.cdecl.} =
  discard sdl2.setClipboardText(text)

proc igSDL2ProcessEvent*(userData: pointer, event: var Event): bool =
  var io = igGetIO()
  case event.kind
  of MouseWheel:
    if event.wheel.x > 0: io.mouseWheelH += 1
    elif event.wheel.x < 0: io.mouseWheelH -= 1
    if event.wheel.y > 0: io.mouseWheel += 1
    elif event.wheel.y < 0: io.mouseWheel -= 1
    result = true
  of MouseButtonDown:
    case event.button.button
    of BUTTON_LEFT: mousePressed[0] = true
    of BUTTON_RIGHT: mousePressed[1] = true
    of BUTTON_MIDDLE: mousePressed[2] = true
    else: discard
    result = true
  of TextInput:
    io.addInputCharactersUTF8(event.text.text.addr)
    result = true
  of KeyDown, KeyUp:
    let key = event.key.keysym.scancode.int32
    assert(key >= 0 and key < io.keysDown.len(), "what the fuck")
    let modState = sdl2.getModState().toInt()
    io.keysDown[key] = (event.kind == KeyDown)
    io.keyShift = ((modState and KMOD_SHIFT) != 0)
    io.keyCtrl = ((modState and KMOD_CTRL) != 0)
    io.keyAlt = ((modState and KMOD_ALT) != 0)
    io.keySuper = false
    result = true
  of WindowEvent:
    if event.window.event == WindowEvent_FocusGained:
      io.addFocusEvent(true)
    elif event.window.event == WindowEvent_FocusLost:
      io.addFocusEvent(false)
    result = true
  else:
    result = false

proc igSDL2Init*(inWindow: WindowPtr): bool =
  var io = igGetIO()
  assert(io.backendPlatformUserData == nil, "already initialized backend")

  ticks = 0

  io.backendPlatformName = platformBackendName.cstring
  io.backendPlatformUserData = inWindow
  io.backendFlags = (io.backendFlags.int32 or ImGuiBackendFlags.HasSetMousePos.int32).ImGuiBackendFlags

  io.setClipboardTextFn = igSDL2SetClipboardText
  io.getClipboardTextFn = igSDL2GetClipboardText
  io.clipboardUserData = nil

  io.keyMap[ImGuiKey.Tab.int32] = SDL_SCANCODE_TAB.int32
  io.keyMap[ImGuiKey.LeftArrow.int32] = SDL_SCANCODE_LEFT.int32
  io.keyMap[ImGuiKey.RightArrow.int32] = SDL_SCANCODE_RIGHT.int32
  io.keyMap[ImGuiKey.UpArrow.int32] = SDL_SCANCODE_UP.int32
  io.keyMap[ImGuiKey.DownArrow.int32] = SDL_SCANCODE_DOWN.int32
  io.keyMap[ImGuiKey.PageUp.int32] = SDL_SCANCODE_PAGEUP.int32
  io.keyMap[ImGuiKey.PageDown.int32] = SDL_SCANCODE_PAGEDOWN.int32
  io.keyMap[ImGuiKey.Home.int32] = SDL_SCANCODE_HOME.int32
  io.keyMap[ImGuiKey.End.int32] = SDL_SCANCODE_END.int32
  io.keyMap[ImGuiKey.Insert.int32] = SDL_SCANCODE_INSERT.int32
  io.keyMap[ImGuiKey.Delete.int32] = SDL_SCANCODE_DELETE.int32
  io.keyMap[ImGuiKey.Backspace.int32] = SDL_SCANCODE_BACKSPACE.int32
  io.keyMap[ImGuiKey.Space.int32] = SDL_SCANCODE_SPACE.int32
  io.keyMap[ImGuiKey.Enter.int32] = SDL_SCANCODE_RETURN.int32
  io.keyMap[ImGuiKey.Escape.int32] = SDL_SCANCODE_ESCAPE.int32
  io.keyMap[ImGuiKey.KeyPadEnter.int32] = SDL_SCANCODE_KP_ENTER.int32
  io.keyMap[ImGuiKey.A.int32] = SDL_SCANCODE_A.int32
  io.keyMap[ImGuiKey.C.int32] = SDL_SCANCODE_C.int32
  io.keyMap[ImGuiKey.V.int32] = SDL_SCANCODE_V.int32
  io.keyMap[ImGuiKey.X.int32] = SDL_SCANCODE_X.int32
  io.keyMap[ImGuiKey.Y.int32] = SDL_SCANCODE_Y.int32
  io.keyMap[ImGuiKey.Z.int32] = SDL_SCANCODE_Z.int32

  window = inWindow

  result = true

proc igSDL2Shutdown*() =
  var io = igGetIO()
  if lastClipboardText != nil:
    freeClipboardText(lastClipboardText)
    lastClipboardText = nil
  window = nil
  io.backendPlatformUserData = nil
  io.backendPlatformName = nil

proc updateMouse() =
  var io = igGetIO()

  let oldPos = io.mousePos
  io.mousePos = ImVec2(x: float32.low, y: float32.low)

  var localX, localY: cint
  let buttons = sdl2.getMouseState(localX, localY)
  io.mouseDown[0] = mousePressed[0] or (buttons and BUTTON_LMASK) != 0
  io.mouseDown[1] = mousePressed[1] or (buttons and BUTTON_RMASK) != 0
  io.mouseDown[2] = mousePressed[2] or (buttons and BUTTON_MMASK) != 0
  mousePressed[0] = false
  mousePressed[1] = false
  mousePressed[2] = false

  # this was under `#if SDL_HAS_CAPTURE_AND_GLOBAL_MOUSE`, but we expect SDL>=2.0.4 and a system with a window manager
  let focusWindow = sdl2.getKeyboardFocus()
  let hoverWindow = sdl2.getMouseFocus()
  var mouseWindow = 
    if hoverWindow != nil and window == hoverWindow:
      hoverWindow
    elif focusWindow != nil and window == focusWindow:
      focusWindow
    else:
      nil

  discard sdl2.captureMouse(igIsAnyMouseDown().Bool32)

  if mouseWindow == nil:
    return

  if io.wantSetMousePos:
    sdl2.warpMouseInWindow(window, oldPos.x.cint, oldPos.y.cint)

  io.mousePos = ImVec2(x: localX.float32, y: localY.float32)

proc igSDL2NewFrame*() =
  var io = igGetIO()

  var w, h, displayW, displayH: cint
  window.getSize(w, h)
  if (window.getFlags() and SDL_WINDOW_MINIMIZED) != 0:
    w = 0
    h = 0
  window.glGetDrawableSize(displayW, displayH) # this defaults to SDL_GetWindowSize if no GL

  io.displaySize = ImVec2(x: w.float32, y: h.float32)
  if w > 0 and h > 0:
    io.displayFramebufferScale = ImVec2(x: displayW.float32 / w.float32, y: displayH.float32 / h.float32)

  let freq = sdl2.getPerformanceFrequency()
  let curTime = sdl2.getPerformanceCounter()
  io.deltaTime =
    if ticks > 0:
      ((curTime - ticks).float64 / freq.float64).float32
    else:
      (1.0 / 60.0).float32

  updateMouse()
