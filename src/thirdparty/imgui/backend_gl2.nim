import
  opengl,
  base

const
  rendererBackendName = "gl2"

var
  fontTexture: GLuint = 0

proc igGL2Init*(): bool =
  var io = igGetIO()
  assert(io.backendRendererUserData == nil, "already initialized a backend")
  io.backendRendererUserData = rendererBackendName.cstring # just to detect whether we've initialized or not
  io.backendRendererName = rendererBackendName
  result = true

proc setupRendererState(drawData: ptr ImDrawData, width, height: int) =
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glDisable(GL_CULL_FACE)
    glDisable(GL_DEPTH_TEST)
    glDisable(GL_STENCIL_TEST)
    glDisable(GL_LIGHTING)
    glDisable(GL_COLOR_MATERIAL)
    glEnable(GL_SCISSOR_TEST)
    glEnableClientState(GL_VERTEX_ARRAY)
    glEnableClientState(GL_TEXTURE_COORD_ARRAY)
    glEnableClientState(GL_COLOR_ARRAY)
    glDisableClientState(GL_NORMAL_ARRAY)
    glEnable(GL_TEXTURE_2D)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    glShadeModel(GL_SMOOTH)
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE.GLint)
    glViewport(0, 0, width.GLsizei, height.GLsizei)
    glMatrixMode(GL_PROJECTION)
    glPushMatrix()
    glLoadIdentity()
    glOrtho(
      drawData.displayPos.x, drawData.displayPos.x + drawData.displaySize.x,
      drawData.displayPos.y + drawData.displaySize.y, drawData.displayPos.y,
      -10.0.GLfloat, +10.0.GLfloat
    )
    glMatrixMode(GL_MODELVIEW)
    glPushMatrix()
    glLoadIdentity()

proc igGL2EndFrame*() =
  var io = igGetIO()
  glViewport(0, 0, io.displaySize.x.GLsizei, io.displaySize.y.GLsizei)

proc igGL2RenderDrawData*(drawData: ptr ImDrawData) =
  let width = int(drawData.displaySize.x * drawData.framebufferScale.x)
  let height = int(drawData.displaySize.y * drawData.framebufferScale.y)
  if width == 0 or height == 0: return

  # backup GL state
  var lastTexture, lastShadeModel, lastTexEnvMode: GLint
  var lastPolygonMode: array[2, GLint]
  var lastViewport: array[4, GLint]
  var lastScissorBox: array[4, GLint]
  glGetIntegerv(GL_TEXTURE_BINDING_2D, lastTexture.addr)
  glGetIntegerv(GL_POLYGON_MODE, lastPolygonMode[0].addr)
  glGetIntegerv(GL_VIEWPORT, lastViewport[0].addr)
  glGetIntegerv(GL_SCISSOR_BOX, lastScissorBox[0].addr)
  glGetIntegerv(GL_SHADE_MODEL, lastShadeModel.addr)
  glGetTexEnviv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, lastTexEnvMode.addr)
  glPushAttrib(GL_ENABLE_BIT or GL_COLOR_BUFFER_BIT or GL_TRANSFORM_BIT)

  # setup new state
  setupRendererState(drawData, width, height)

  # project scissor/clipping rectangles into framebuffer space
  let clipOff = drawData.displayPos
  let clipScale = drawData.framebufferScale

  # render command lists
  for n in 0..<drawData.cmdListsCount:
    let cmdList = drawData.cmdLists[n]
    glVertexPointer(2.GLint, cGL_FLOAT, sizeof(ImDrawVert).GLsizei, cmdList.vtxBuffer.data[0].pos.addr)
    glTexCoordPointer(2.GLint, cGL_FLOAT, sizeof(ImDrawVert).GLsizei, cmdList.vtxBuffer.data[0].uv.addr)
    glColorPointer(4.GLint, cGL_UNSIGNED_BYTE, sizeof(ImDrawVert).GLsizei, cmdList.vtxBuffer.data[0].col.addr)
    for cmdNum in 0..<cmdList.cmdBuffer.size:
      var cmd = cmdList.cmdBuffer.data[cmdNum]
      if cmd.userCallback != nil:
        if cast[int](cmd.userCallback) == -1:
          setupRendererState(drawData, width, height) # ImDrawCallback_ResetRenderState
        else:
          cmd.userCallback(cmdList, cmd.addr)
      else:
        var clipRect = ImVec4(
          x: (cmd.clipRect.x - clipOff.x) * clipScale.x,
          y: (cmd.clipRect.y - clipOff.y) * clipScale.y,
          z: (cmd.clipRect.z - clipOff.x) * clipScale.x,
          w: (cmd.clipRect.w - clipOff.y) * clipScale.y
        )
        if clipRect.x < width.float32 and clipRect.y < height.float32 and clipRect.z >= 0.0f and clipRect.w >= 0.0f:
          glScissor(clipRect.x.int32, (height.float32 - clipRect.w).int32, (clipRect.z - clipRect.x).int32, (clipRect.w - clipRect.y).int32)
          glBindTexture(GL_TEXTURE_2D, cast[uint32](cmd.textureId))
          glDrawElements(GL_TRIANGLES, cmd.elemCount.int32, if ImDrawIdx.sizeof() == 2: cGL_UNSIGNED_SHORT else: GL_UNSIGNED_INT, cmdList.idxBuffer.data[cmd.idxOffset].addr)

  # restore old GL state

  glDisableClientState(GL_COLOR_ARRAY)
  glDisableClientState(GL_TEXTURE_COORD_ARRAY)
  glDisableClientState(GL_VERTEX_ARRAY)
  glBindTexture(GL_TEXTURE_2D, lastTexture.GLuint)
  glMatrixMode(GL_MODELVIEW)
  glPopMatrix()
  glMatrixMode(GL_PROJECTION)
  glPopMatrix()
  glPopAttrib()
  glPolygonMode(GL_FRONT, lastPolygonMode[0].GLenum)
  glPolygonMode(GL_BACK, lastPolygonMode[1].GLenum)
  glViewport(lastViewport[0], lastViewport[1], lastViewport[2].GLsizei, lastViewport[3].GLsizei)
  glScissor(lastScissorBox[0], lastScissorBox[1], lastScissorBox[2].GLsizei, lastScissorBox[3].GLsizei)
  glShadeModel(lastShadeModel.GLenum)
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, lastTexEnvMode)

proc createFontTexture() =
  var io = igGetIO()
  var textPixels: ptr uint8 = nil
  var textW: int32
  var textH: int32
  io.fonts.getTexDataAsRGBA32(textPixels.addr, textW.addr, textH.addr)

  var lastTexture: GLint
  glGetIntegerv(GL_TEXTURE_BINDING_2D, lastTexture.addr)
  glGenTextures(1, fontTexture.addr)
  glBindTexture(GL_TEXTURE_2D, fontTexture)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
  glPixelStorei(GL_UNPACK_ROW_LENGTH, 0)
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA.GLint, textW.GLsizei, textH.GLsizei, 0, GL_RGBA, GL_UNSIGNED_BYTE, textPixels)

  io.fonts.texID = cast[ImTextureID](fontTexture)
  glBindTexture(GL_TEXTURE_2D, lastTexture.GLuint)


proc destroyFontTexture() =
  if fontTexture > 0'u32:
    let io = igGetIO()
    glDeleteTextures(1, fontTexture.addr)
    io.fonts.texID = cast[ImTextureID](0)
    fontTexture = 0

proc igGL2NewFrame*() =
  if fontTexture == 0.GLuint:
    createFontTexture()

proc igGL2Shutdown*() =
  var io = igGetIO()
  assert(io.backendRendererUserData != nil, "backend not initialized")
  destroyFontTexture()
  io.backendRendererUserData = nil
  io.backendRendererName = nil
