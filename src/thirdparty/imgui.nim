import
  std/logging, std/sequtils, std/strutils,
  sdl2, opengl

import
  imgui/base, imgui/backend_sdl2, imgui/backend_gl2

export
  base

var
  imguiCtx*: ptr ImGuiContext = nil

proc init*(window: WindowPtr): bool =
  var context = igCreateContext()
  igStyleColorsDark()
  if not igSDL2Init(window):
    error("Could not init SDL2 backend for ImGui: ", sdl2.getError())
    context.igDestroyContext()
    return false
  if not igGL2Init():
    error("Could not init GL2 backend for ImGui")
    context.igDestroyContext()
    return false
  imguiCtx = context
  result = true

proc shutdown*() =
  if imguiCtx != nil:
    igGL2Shutdown()
    igSDL2Shutdown()
    imguiCtx.igDestroyContext()
    imguiCtx = nil

proc beginFrame*() =
  igGL2NewFrame()
  igSDL2NewFrame()
  igNewFrame()

proc endFrame*() =
  igRender()
  igGL2EndFrame()
  igGL2RenderDrawData(igGetDrawData())

proc event*(event: var Event): bool =
  result = igSDL2ProcessEvent(nil, event)

# operators on ImVec2

proc `+`*(a, b: ImVec2): ImVec2 = ImVec2(x: a.x + b.x, y: a.y + b.y)
proc `-`*(a, b: ImVec2): ImVec2 = ImVec2(x: a.x - b.x, y: a.y - b.y)
proc `*`*(a: ImVec2, b: float32): ImVec2 = ImVec2(x: a.x * b, y: a.y * b)
proc `/`*(a: ImVec2, b: float32): ImVec2 = ImVec2(x: a.x / b, y: a.y / b)

# operators on flags

proc `or`*(a, b: ImGuiWindowFlags): ImGuiWindowFlags = ImGuiWindowFlags(a.int32 or b.int32)
proc `or`*(a, b: ImGuiItemFlags): ImGuiItemFlags = ImGuiItemFlags(a.int32 or b.int32)

# extra helper functions

proc rgba*(r, g, b, a: float32): ImVec4 =
  ImVec4(x: r, y: g, z: b, w: a)

proc pushInactive*() =
  igPushItemFlag(ImGuiItemFlags.Disabled, true)
  igPushStyleVar(ImGuiStyleVar.Alpha, igGetStyle().alpha * 0.5f)

proc popInactive*() =
  igPopStyleVar()
  igPopItemFlag()

# control wrappers; need these mostly because of a weird fucking header error and also for type checking convenience

proc showStackToolWindow*() =
  igShowStackToolWindow()

proc beginWindow*(label: string, pOpen: ptr bool = nil, flags: ImGuiWindowFlags = ImGuiWindowFlags.None): bool =
  igBegin(label.cstring, pOpen, flags)

proc endWindow*() =
  igEnd()

proc openPopup*(label: string, flags: ImGuiPopupFlags = ImGuiPopupFlags.None) =
  igOpenPopup(label.cstring, flags)

proc beginPopup*(label: string, flags: ImGuiWindowFlags = ImGuiWindowFlags.None): bool =
  igBeginPopup(label.cstring, flags)

proc endPopup*() =
  igEndPopup()

proc closeCurrentPopup*() =
  igCloseCurrentPopup()

proc beginPopupModal*(label: string, pOpen: ptr bool = nil, flags: ImGuiWindowFlags = ImGuiWindowFlags.None): bool =
  igBeginPopupModal(label.cstring, pOpen, flags)

proc beginMenu*(label: string, enabled: bool = true): bool =
  igBeginMenu(label.cstring, enabled)

proc endMenu*() =
  igEndMenu()

proc beginMenuBar*(): bool =
  igBeginMenuBar()

proc endMenuBar*() =
  igEndMenuBar()

proc beginMainMenuBar*(): bool =
  igBeginMainMenuBar()

proc endMainMenuBar*() =
  igEndMainMenuBar()

proc beginTable*(tableId: string, columns: int, flags: ImGuiTableFlags = ImGuiTableFlags.None, outerSz: ImVec2 = ImVec2(x: 0.0f, y: 0.0f), innerW: float32 = 0.0f): bool =
  igBeginTable(tableId.cstring, columns.int32, flags, outerSz, innerW)

proc endTable*() =
  igEndTable()

proc beginListBox*(label: string, size: ImVec2 = ImVec2(x: 0, y: 0)): bool =
  igBeginListBox(label.cstring, size)

proc endListBox*() =
  igEndListBox()

proc beginCombo*(label: string, preview: string, flags: ImGuiComboFlags = ImGuiComboFlags.None): bool =
  igBeginCombo(label.cstring, preview.cstring, flags)

proc endCombo*() =
  igEndCombo()

proc beginChild*(label: string, size: ImVec2 = ImVec2(x: 0, y: 0), border: bool = false, flags: ImGuiWindowFlags = ImGuiWindowFlags.None): bool {.discardable.} =
  igBeginChild(label.cstring, size, border, flags)

proc endChild*() =
  igEndChild()

proc treeNode*(label: string): bool =
  igTreeNode(label.cstring)

proc treePop*() =
  igTreePop()

proc separator*() =
  igSeparator()

proc sameLine*(offsetX: float32 = 0f, spacing: float32 = -1f) =
  igSameLine(offsetX, spacing)

proc dummy*(size: ImVec2) =
  igDummy(size)

proc text*(args: varargs[string, `$`]) =
  var argStr = args.join()
  igText("%s", argStr.cstring)

proc image*(id: uint32, size: ImVec2, borderCol: ImVec4 = ImVec4(x: 0, y: 0, z: 0, w: 0)) =
  igImage(cast[ImTextureId](id), size, border_col = borderCol)

proc textWrapped*(args: varargs[string, `$`]) =
  var argStr = args.join()
  igTextWrapped("%s", argStr.cstring)

proc button*(label: string, size: ImVec2 = ImVec2(x: 0, y: 0)): bool =
  igButton(label.cstring, size)

proc arrowButton*(label: string, dir: ImGuiDir): bool =
  igArrowButton(label.cstring, dir)

proc arrowButtonEx*(label: string, dir: ImGuiDir, size: ImVec2, flags: ImGuiButtonFlags = ImGuiButtonFlags.None): bool =
  igArrowButtonEx(label.cstring, dir, size, flags)

proc checkbox*(label: string, v: var bool): bool {.discardable.} =
  igCheckbox(label.cstring, v.addr)

proc selectable*(label: string, selected: bool = false, flags: ImGuiSelectableFlags = ImGuiSelectableFlags.None, size: ImVec2 = ImVec2(x: 0, y: 0)): bool {.discardable.} =
  igSelectable(label.cstring, selected, flags, size)

proc combo*(label: string, index: var int, choices: openArray[string]): bool =
  var choicesPtrs = newSeq[cstring](choices.len())
  var curChoice = index.int32
  for i, x in choices: choicesPtrs[i] = x.cstring
  result = igCombo(label.cstring, curChoice.addr, choicesPtrs[0].addr, choices.len().int32)
  index = curChoice.int

proc menuItem*(label: string, shortcut: string = "", selected: bool = false, enabled: bool = true): bool {.discardable.} =
  igMenuItem(label.cstring, (if shortcut == "": nil else: shortcut.cstring), selected, enabled)

proc menuItem*(label: string, shortcut: string = "", pSelected: ptr bool, enabled: bool = true): bool {.discardable.} =
  igMenuItem(label.cstring, (if shortcut == "": nil else: shortcut.cstring), pSelected, enabled)

proc inputText*(label: string, outStr: var string, outMaxLen: int = 0, flags: ImGuiInputTextFlags = ImGuiInputTextFlags.None, callback: ImGuiInputTextCallback = nil, userData: pointer = nil): bool =
  var strMax = (if outMaxLen == 0: outStr.len() else: outMaxLen)
  if strMax == 0: strMax = 1 # for the \0
  var tmp = outStr.items().toSeq()
  tmp.setLen(strMax)
  if igInputText(label.cstring, tmp[0].addr.cstring, tmp.len().uint, flags, callback, userData):
    outStr = $(tmp[0].addr.cstring)
    result = true
  else:
    result = false

proc inputInt*(label: string, x: var int32, step: int32 = 1, stepFast: int32 = 100, flags: ImGuiInputTextFlags = ImGuiInputTextFlags.None): bool =
  igInputInt(label.cstring, x.addr, step, stepFast, flags)

proc inputInt2*(label: string, x: var array[2, int32], flags: ImGuiInputTextFlags = ImGuiInputTextFlags.None): bool =
  igInputInt2(label.cstring, x[0].addr, flags)

proc inputInt4*(label: string, x: var array[4, int32], flags: ImGuiInputTextFlags = ImGuiInputTextFlags.None): bool =
  igInputInt4(label.cstring, x[0].addr, flags)

proc columns*(count: int = 1, border: bool = true) =
  igColumns(count.int32, nil, border)

proc nextColumn*() =
  igNextColumn()

proc setColumnWidth*(idx: int, w: float32) =
  igSetColumnWidth(idx.int32, w)

proc alignTextToFramePadding*() =
  igAlignTextToFramePadding()

proc setItemDefaultFocus*() =
  igSetItemDefaultFocus()

proc setNextItemWidth*(w: float32) =
  igSetNextItemWidth(w)

proc setNextWindowPos*(pos: ImVec2, cond: ImGuiCond = ImGuiCond.None, pivot: ImVec2 = ImVec2(x: 0, y: 0)) =
  igSetNextWindowPos(pos, cond, pivot)

proc setNextWindowSize*(size: ImVec2, cond: ImGuiCond = ImGuiCond.None) =
  igSetNextWindowSize(size, cond)

proc setNextWindowContentSize*(size: ImVec2) =
  igSetNextWindowContentSize(size)

proc setNextWindowSizeConstraints*(minSize, maxSize: ImVec2) =
  igSetNextWindowSizeConstraints(minSize, maxSize)

proc setCursorPosX*(x: float32) =
  igSetCursorPosX(x)

proc setScrollFromPosX*(x: float32, ratio: float32 = 0.5f) =
  igSetScrollFromPosX(x, ratio)

proc setScrollFromPosY*(y: float32, ratio: float32 = 0.5f) =
  igSetScrollFromPosY(y, ratio)

proc setScrollHereX*(ratio: float32 = 0.5f) =
  igSetScrollHereX(ratio)

proc setScrollHereY*(ratio: float32 = 0.5f) =
  igSetScrollHereY(ratio)

proc setScrollX*(ratio: float32) =
  igSetScrollX(ratio)

proc setScrollY*(ratio: float32) =
  igSetScrollY(ratio)

proc getScrollX*(): float32 =
  igGetScrollX()

proc getScrollY*(): float32 =
  igGetScrollY()

proc getScrollMaxX*(): float32 =
  igGetScrollMaxX()

proc getScrollMaxY*(): float32 =
  igGetScrollMaxY()

proc getItemSize*(): ImVec2 =
  igGetItemRectSizeNonUDT(result.addr)

proc getWindowSize*(): ImVec2 =
  igGetWindowSizeNonUDT(result.addr)

proc getWindowContentRegionSize*(): ImVec2 =
  igGetWindowContentRegionMaxNonUDT(result.addr)
  var mins: ImVec2
  igGetWindowContentRegionMinNonUDT(mins.addr)
  result = result - mins

proc getIO*(): ptr ImGuiIO =
  igGetIO()

proc getStyle*(): ptr ImGuiStyle =
  igGetStyle()

proc getFrameHeight*(): float32 =
  igGetFrameHeight()

proc getFrameHeightWithSpacing*(): float32 =
  igGetFrameHeightWithSpacing()

proc getCursorPosX*(): float32 =
  igGetCursorPosX()

proc getCursorPosY*(): float32 =
  igGetCursorPosY()

proc getCursorPos*(): ImVec2 =
  ImVec2(x: igGetCursorPosX(), y: igGetCursorPosY())

proc getMousePos*(): ImVec2 =
  igGetMousePosNonUDT(result.addr)

proc calcTextSize*(text: string): ImVec2 =
  igCalcTextSizeNonUDT(result.addr, text.cstring)

proc pushStyleColor*(what: ImGuiCol, col: ImVec4) =
  igPushStyleColor(what, col)

proc popStyleColor*(count: int = 1) =
  igPopStyleColor(count.int32)

proc pushItemWidth*(w: float32) =
  igPushItemWidth(w)

proc popItemWidth*() =
  igPopItemWidth()

proc isItemFocused*(): bool =
  igIsItemFocused()

proc isItemActive*(): bool =
  igIsItemActive()

proc isPopupOpen*(id: string): bool =
  igIsPopupOpen(id.cstring)

proc isMouseDoubleClicked*(button: ImGuiMouseButton): bool =
  igIsMouseDoubleClicked(button)

proc isMouseDown*(button: ImGuiMouseButton): bool =
  igIsMouseDown(button)

proc isMouseClicked*(button: ImGuiMouseButton): bool =
  igIsMouseClicked(button)

proc isKeyPressed*(key: ImGuiKey, repeat: bool = true): bool =
  igIsKeyPressedMap(key, repeat)

proc isAnyModalPopupOpen*(): bool =
  (igGetTopMostPopupModal() != nil)

# `with`-style templates for all kinds of controls

template doWindow*(label: string, actions: untyped): untyped =
  if imgui.beginWindow(label):
    actions
    imgui.endWindow()

template doPopup*(label: string, actions: untyped): untyped =
  if imgui.beginPopup(label):
    actions
    imgui.endPopup()

template doPopupModal*(label: string, actions: untyped): untyped =
  if imgui.beginPopupModal(label):
    actions
    imgui.endPopup()

template doMenu*(label: string, actions: untyped): untyped =
  if imgui.beginMenu(label):
    actions
    imgui.endMenu()

template doMenuBar*(actions: untyped): untyped =
  if imgui.beginMenuBar():
    actions
    imgui.endMenuBar()

template doMainMenuBar*(actions: untyped): untyped =
  if imgui.beginMainMenuBar():
    actions
    imgui.endMainMenuBar()

template doTreeNode*(label: string, actions: untyped): untyped =
  if imgui.treeNode(label):
    actions
    imgui.treePop()

template doTable*(label: string, colums: int, actions: untyped): untyped =
  if imgui.beginTable(label, columns):
    actions
    imgui.endTable()

template doCombo*(label: string, initVal: string, actions: untyped): untyped =
  if imgui.beginCombo(label, initVal):
    actions
    imgui.endCombo()

template doListBox*(label: string, actions: untyped): untyped =
  if imgui.beginListBox(label):
    actions
    imgui.endListBox()

template doChild*(label: string, actions: untyped): untyped =
  if imgui.beginChild(label):
    actions
    imgui.endChild()

template doInactive*(actions: untyped): untyped =
  imgui.pushInactive()
  actions
  imgui.popInactive()

template doColumns*(numCols: int, border: bool, actions: untyped): untyped =
  imgui.columns(numCols, border)
  actions
  imgui.columns(1)
