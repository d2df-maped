import
  std/macros, std/strutils, std/enumutils, std/strscans, std/sets, std/streams,
  std/logging, std/exitprocs, std/tables

export
  logging, exitprocs

type
  DFPoint* = tuple[x, y: int]
  DFSize* = tuple[w, h: int]
  DFColor* = tuple[r, g, b, a: uint8]

proc initDFPoint*(x, y: int): DFPoint =
  result.x = x
  result.y = y

proc initDFSize*(x, y: int): DFSize =
  result.w = x
  result.h = y

proc initDFColor*(r, g, b, a: uint8): DFColor =
  result.r = r
  result.g = g
  result.b = b
  result.a = a

proc initDFColor*(r, g, b, a: int): DFColor =
  result.r = r.uint8
  result.g = g.uint8
  result.b = b.uint8
  result.a = a.uint8

proc x*(self: var DFSize): var int = self.w
proc x*(self: DFSize): int = self.w
proc y*(self: var DFSize): var int = self.h
proc y*(self: DFSize): int = self.h

proc component*(self: DFPoint | DFSize, i: int): int =
  case i
  of 0: self.x
  of 1: self.y
  else: raise newException(IndexDefect, "a DFPoint/DFSize only has 2 components, requested " & $i)

proc parseDFPoint*(str: string): DFPoint =
  if not scanf(str, "($s$i $i$s)", result.x, result.y):
    raise newException(ValueError, str & " is not a valid vec2")

proc parseDFSize*(str: string): DFSize =
  if not scanf(str, "($s$i $i$s)", result.w, result.h):
    raise newException(ValueError, str & " is not a valid vec2")

proc parseDFColor*(str: string): DFColor =
  var x, y, z, w: int
  if not scanf(str, "($s$i $i $i $i$s)", x, y, z, w):
    raise newException(ValueError, str & " is not a valid vec4")
  return initDFColor(x.uint8, y.uint8, z.uint8, w.uint8)

proc `$`*(v: DFPoint): string = "($1 $2)" % [$v.x, $v.y]

proc `$`*(v: DFSize): string = "($1 $2)" % [$v.w, $v.h]

proc `$`*(v: DFColor): string = "($1 $2 $3 $4)" % [$v.r, $v.g, $v.b, $v.a]

iterator components*(v: DFPoint): int =
  yield v.x
  yield v.y

iterator mcomponents*(v: var DFPoint): var int =
  yield v.x
  yield v.y

iterator components*(v: DFSize): int =
  yield v.w
  yield v.h

iterator mcomponents*(v: var DFSize): var int =
  yield v.w
  yield v.h

iterator components*(v: DFColor): uint8 =
  yield v.r
  yield v.g
  yield v.b
  yield v.a

iterator mcomponents*(v: var DFColor): var uint8 =
  yield v.r
  yield v.g
  yield v.b
  yield v.a

proc `+`*(a, b: DFPoint): DFPoint = (a.x + b.x, a.y + b.y)
proc `+`*(a, b: DFSize): DFSize = (a.w + b.w, a.h + b.h)
proc `+`*(a: DFPoint, b: DFSize): DFPoint = (a.x + b.w, a.y + b.h)

proc `-`*(a, b: DFPoint): DFPoint = (a.x - b.x, a.y - b.y)
proc `-`*(a, b: DFSize): DFSize = (a.w - b.w, a.h - b.h)
proc `-`*(a: DFPoint, b: DFSize): DFPoint = (a.x - b.w, a.y - b.h)

proc `*`*(a: DFPoint, b: int): DFPoint = (a.x * b, a.y * b)
proc `*`*(a: DFSize, b: int): DFSize = (a.x * b, a.y * b)

proc `div`*(a: DFPoint, b: int): DFPoint = (a.x div b, a.y div b)
proc `div`*(a: DFSize, b: int): DFSize = (a.x div b, a.y div b)

proc parseDFBool*(str: string): bool =
  let s = str.toLowerAscii()
  if s == "true" or s == "yes" or s == "tan" or s == "1":
    return true
  if s == "false" or s == "no" or s == "ona" or s == "0":
    return false
  raise newException(ValueError, "invalid bool value: '$1'" % [str])

# value name for empty bitset
template bitsetNullValue*(name: string) {.pragma.}

proc bitsetToString*[T: enum](value: set[T]): string =
  result = "";
  for e in T.items:
    if e in value:
      if result != "":
        result &= " | "
      result &= $e
  when T.hasCustomPragma(bitsetNullValue):
    if result == "":
      result = T.getCustomPragmaVal(bitsetNullValue)

proc parseBitSet*[T: enum](str: string): set[T] =
  result = { }
  for tokDirty in str.split('|'):
    let tok = tokDirty.strip()
    when T.hasCustomPragma(bitsetNullValue):
      if tok == T.getCustomPragmaVal(bitsetNullValue):
        continue # skip zero value for this enum type
    result.incl(parseEnum[T](tok))

proc quote*(str: string, qchar: char = '\''): string =
  # strutils.escape escapes a bit too much shit and murders utf-8, so here goes
  result = $qchar
  for ch in str:
    if ch == qchar:
      result &= "\\" & ch
    else:
      result &= ch
  result &= qchar

proc unquote*(str: string): string =
  # same here
  if str.len() != 0:
    let qchar = str[0]
    if qchar in { '\'', '"' }:
      if str.endsWith(qchar):
        let size = str.len() - 1
        result = ""
        for i in 1..<size:
          let ch = str[i]
          # skip the \ in \"
          if not (ch == '\\' and i < size - 1 and str[i + 1] == qchar):
            result &= ch
        return
  raise newException(ValueError, "invalid quoted string: `" & str & "`")

proc snakeCase*(str: string): string =
  result = ""
  for ch in str:
    if ch.isUpperAscii():
      result &= '_' & ch.toLowerAscii()
    else:
      result &= ch

proc raiseBaseMethodCall*(name: string) =
  raise newException(CatchableError, "pure virtual call: " & name)

macro inspectAST*(ast: untyped) =
  echo(ast.treeRepr())

proc toString*[N](x: array[N, char]): string =
  result = newStringOfCap(x.len())
  for ch in x:
    if ch == '\0':
      break
    result &= ch

proc toArray*[N](x: string, arr: var array[N, char]) =
  for i, och in arr.mpairs():
    if i < x.len():
      och = x[i]
    else:
      och = '\0'

proc skip*(self: Stream, bytes: int) =
  self.setPosition(self.getPosition() + bytes)

proc normalizeDFPath*(s: string): string =
  result = newStringOfCap(s.len())
  for ch in s:
    if ch == '\\':
      result &= '/'
    else:
      result &= ch

proc filenameCmp*(x, y: string): int =
  if x == "..": -1
  elif y == "..": 1
  elif x.endsWith("/") and not y.endsWith("/"): -1
  elif y.endsWith("/") and not x.endsWith("/"): 1
  else: system.cmp(x, y)

proc readDFConfig*(stream: Stream): Table[string, string] =
  result = initTable[string, string]()
  var curSection = ""
  for origLine in stream.lines():
    let line = origLine.strip()
    if line == "" or line[0] == ';':
      discard # skip whitespace and comments
    elif line[0] == '[' and line.endsWith(']'):
      curSection = line[1 .. ^2].strip().toLowerAscii()
    else:
      let tokens = line.split('=', 1)
      if tokens.len() == 2:
        var name = curSection
        name.addSep(".")
        name &= tokens[0].strip().toLowerAscii()
        result[name] = tokens[1].strip()

proc intersect*(pos1: DFPoint, size1: DFSize, pos2: DFPoint, size2: DFSize): bool =
  (pos1.x < pos2.x + size2.w) and
  (pos2.x < pos1.x + size1.w) and
  (pos1.y < pos2.y + size2.h) and
  (pos2.y < pos1.y + size1.h)

proc intersect*(pt: DFPoint, rpos: DFPoint, rsize: DFSize): bool =
  (pt.x > rpos.x and pt.x < rpos.x + rsize.w) and
  (pt.y > rpos.y and pt.y < rpos.y + rsize.h)
