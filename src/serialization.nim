import
  std/macros,
  serialization/[common, binary, text],
  utils

export
  common, binary, text

type
  SomeSerializer* = BinSerializer | TextSerializer

# this is only used for triggerdata, everything else is done at compile time
method serializeDynamic*(self: Serializable, stream: BinSerializer) {.base.} = raiseBaseMethodCall("serializeDynamic")
method serializeDynamic*(self: Serializable, stream: TextSerializer) {.base.} = raiseBaseMethodCall("serializeDynamic")

proc serialize*[T: Serializable](self: SomeSerializer, blockName: string, blockId: int, value: T) =
  if self.beginBlockType(blockName, blockId):
    if self.beginBlockItem(blockName, blockId, value):
      when value.hasCustomPragma(triggerData):
        value.serializeDynamic(self) # each triggerdata subtype can provide its own serialization method
      else:
        self.serialize(value)
      self.endBlockItem()
    self.endBlockType()

proc serialize*[T: Serializable](self: SomeSerializer, blockName: string, blockId: int, values: ref seq[T]) =
  # in text format each object has its own block, while in binary they're grouped in big blocks
  if self.isWriting and values[].len() == 0:
    return  # don't write empty blocks
  if self.beginBlockType(blockName, blockId):
    if self.isWriting:
      for v in values[].mitems():
        if self.beginBlockItem(blockName, blockId, v):
          self.serialize(v)
          self.endBlockItem()
    else:
      # scan until we stop getting blocks of requested type
      # TODO: this entire autistic scheme doesn't support putting different block types and fields out of order
      values[].setLen(0)
      while self.inBlockType(blockName, blockId):
        let oldLen = values[].len()
        values[].setLen(oldLen + 1)
        values[oldLen] = new(T)
        values[oldLen].init()
        values[oldLen].id = cast[type(values[oldLen].id)](oldLen) # default to index-based names in case we're reading from binary
        values[oldLen].name = blockName & $oldLen
        if self.beginBlockItem(blockName, blockId, values[oldLen]):
          self.serialize(values[oldLen])
          self.endBlockItem()
    self.endBlockType()

# generic proc for serializing records
proc serialize*[T: Serializable](stream: SomeSerializer, self: T) =
  for origFieldName, fieldSym in fieldPairs(self[]):
    when not (fieldSym.hasCustomPragma(serializeNever) or (stream is BinSerializer and fieldSym.hasCustomPragma(serializeTextOnly))):
      # rename field if needed
      when fieldSym.hasCustomPragma(serializedFieldName):
        const fieldName = fieldSym.getCustomPragmaVal(serializedFieldName)
      elif origFieldName == "kind":
        const fieldName = "type" # HACK: `type` is reserved, so instead of spamming pragmas everywhere we do this
      else:
        const fieldName = origFieldName.snakeCase()
      # check if we should alaways expect this field to be there
      when fieldSym.hasCustomPragma(writeDefault):
        let mandatory = (stream is BinSerializer) or stream.isWriting # all binary fields are mandatory
      else:
        when fieldSym.hasCustomPragma(defaultValue):
          let defaultVal = cast[type(fieldSym)](fieldSym.getCustomPragmaVal(defaultValue))
          if not stream.isWriting: fieldSym = defaultVal # preinit with custom default value if there is one
        else:
          let defaultVal = default(type(fieldSym))
        let mandatory = (stream is BinSerializer) or (stream.isWriting and fieldSym != defaultVal)
      # actual serialization
      when fieldSym is string:
        when fieldSym.hasCustomPragma(maxSize):
          const strSize = fieldSym.getCustomPragmaVal(maxSize)
        else:
          const strSize = 256 # FIXME
        if stream.isWriting or mandatory or stream.checkNextField(fieldName):
          stream.serialize(fieldName, fieldSym, strSize)
          when fieldSym.hasCustomPragma(resPath):
            # normalize resource paths when reading them
            if not stream.isWriting:
              fieldSym = fieldSym.normalizeDFPath()
      elif fieldSym is ref seq:
        when fieldSym.hasCustomPragma(serializedBlockId):
          const blkId = fieldSym.getCustomPragmaVal(serializedBlockId)
        else:
          const blkId = -1
        when fieldSym.hasCustomPragma(serializedBlockName):
          const blkName = fieldSym.getCustomPragmaVal(serializedBlockName)
        else:
          const blkName = ""
        stream.serialize(blkName, blkId, fieldSym)
      elif fieldSym is Serializable:
        when fieldSym.hasCustomPragma(serializedBlockId):
          const blkId = fieldSym.getCustomPragmaVal(serializedBlockId)
        else:
          const blkId = -1
        when fieldSym.hasCustomPragma(serializedBlockName):
          const blkName = fieldSym.getCustomPragmaVal(serializedBlockName)
        else:
          const blkName = ""
        when blkId != -1 or blkName != "":
          # this is a header or something to that effect; they're always mandatory
          if not stream.isWriting and fieldSym == nil:
            # create and default-init object if it doesn't exist yet
            when fieldSym.hasCustomPragma(triggerData):
              fieldSym = self.newTriggerData() # call parent to provide appropriate triggerdata for us
            else:
              new(fieldSym) # let the type system figure that shit out
            fieldSym.init()
          stream.serialize(blkName, blkId, fieldSym)
        else:
          # this is a direct reference to another serializable
          if mandatory or (not stream.isWriting and stream.checkNextField(fieldName)):
            # HACK: if reading make a placeholder because this stupid fucking macro doesn't provide `var T` fields
            if not stream.isWriting and fieldSym == nil:
              new(fieldSym)
              fieldSym.placeholder = true
            stream.serializeId(fieldName, fieldSym)
      elif not (fieldSym is ref):
        # primitive type
        if mandatory or (not stream.isWriting and stream.checkNextField(fieldName)):
          stream.serialize(fieldName, fieldSym)
          # sometimes shit is misaligned, so padding is needed
          when stream is BinSerializer and fieldSym.hasCustomPragma(overrideSize):
            stream.zeroPad(fieldSym.getCustomPragmaVal(overrideSize) - sizeof(fieldSym))
