import
  std/tables,
  serialization, utils, gfx, vfs,
  map/def, map/entities

export
  def, entities

proc init*() =
  # load entity textures
  for entName, (_, _, _, frameWidth, texName) in mapEntInfo:
    gfx.loadTextureExt(texName, "$$" & entName, (frameWidth, 0))

proc addTexture*(tex: Texture): MapTexture {.discardable.} =
  assert(currentMap != nil, "No map loaded")
  result = new(MapTexture)
  result.id = currentMap.textures[].len().uint16
  result.tex = tex
  result.path = tex.path
  result.animated = (tex.frames > 1)
  currentMap.textures[].add(result)

proc addTexture*(path: string): MapTexture =
  var tex = gfx.getTexture(path)
  if tex == nil:
    raise newException(FsReadError, "Could not load texture: " & path)
  result = map.addTexture(tex)

proc close*() =
  if currentMap != nil:
    currentMap.destroy()
    currentMap = nil

proc create*() =
  if currentMap != nil:
    map.close()
  currentMap = newMap()

proc load*(stream: SomeSerializer) =
  if currentMap != nil:
    map.close()
  currentMap = newMap(stream)
  currentMap.fixupReferences()
  mapForAllRecords(x):
    x.init()

proc select*(localPath: string) =
  if vfs.mapArchive == nil:
    return # nothing to do
  var f = vfs.mapArchive.readFile(localPath)
  if f == nil:
    raise newException(FsReadError, "Could not load map from " & localPath)
  if f.isBinaryFormat():
    map.load(newBinSerializer(localPath, f, false))
  else:
    map.load(newTextSerializer(localPath, f, false))
  f.close()

proc save*(stream: SomeSerializer) =
  assert(currentMap != nil, "No map loaded")
  currentMap.save(stream)

proc reopen*() =
  if currentMap != nil:
    discard
