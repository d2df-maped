import
  std/typetraits, std/macros, std/tables,
  ../serialization, ../utils, ../gfx,
  entities

type
  MapDirection* {.size: sizeof(uint8).} = enum
    dirLeft = "DIR_LEFT"
    dirRight = "DIR_RIGHT"
    dirUnspecified = "DIR_SOMETHING2" # ???

  MapKey* {.size: sizeof(uint8), bitsetNullValue: "KEY_NONE".} = enum
    keyRed = "KEY_RED"
    keyGreen = "KEY_GREEN"
    keyBlue = "KEY_BLUE"
    keyRedTeam = "KEY_REDTEAM"
    keyBlueTeam = "KEY_BLUETEAM"

  MapSpawnEffect* {.size: sizeof(uint8).} = enum
    effNone = "EFFECT_NONE"
    effTeleport = "EFFECT_TELEPORT"
    effRespawn = "EFFECT_RESPAWN"
    effFire = "EFFECT_FIRE"

  MapHitType* {.size: sizeof(uint8).} = enum
    hitSome = "HIT_SOME"
    hitRocket = "HIT_ROCKET"
    hitBFG = "HIT_BFG"
    hitTrap = "HIT_TRAP"
    hitFall = "HIT_FALL"
    hitWater = "HIT_WATER"
    hitAcid = "HIT_ACID"
    hitElectro = "HIT_ELECTRO"
    hitFlame = "HIT_FLAME"
    hitSelf = "HIT_SELF"
    hitDiscon = "HIT_DISCON"

  MapObject* = ref object of Serializable

  MapSpecialTexture* {.size: sizeof(int32).} = enum
    texSpecialNone = (-4, "TEXTURE_SPECIAL_NONE")
    texSpecialAcid2 = (-3, "TEXTURE_SPECIAL_ACID2")
    texSpecialAcid1 = (-2, "TEXTURE_SPECIAL_ACID1")
    texSpecialWater = (-1, "TEXTURE_SPECIAL_WATER")

  MapTexture* = ref object of Serializable
    path* {.writeDefault, resPath, maxSize: 64.}: string
    animated* {.nonEditable.}: bool
    # editor-specific fields
    id* {.serializeNever, nonEditable, invisible.}: uint16
    tex* {.serializeNever, nonEditable, invisible.}: Texture

  # this is defined as if it was a bitfield, but it actually isn't
  MapPanelType* {.size: sizeof(uint16).} = enum
    panNone = (0, "PANEL_NONE")
    panWall = (1, "PANEL_WALL")
    panBack = (2, "PANEL_BACK")
    panFore = (4, "PANEL_FORE")
    panWater = (8, "PANEL_WATER")
    panAcid1 = (16, "PANEL_ACID1")
    panAcid2 = (32, "PANEL_ACID2")
    panStep = (64, "PANEL_STEP")
    panLiftUp = (128, "PANEL_LIFTUP")
    panLiftDown = (256, "PANEL_LIFTDOWN")
    panOpenDoor = (512, "PANEL_OPENDOOR")
    panClosedDoor = (1024, "PANEL_CLOSEDOOR")
    panBlockMon = (2048, "PANEL_BLOCKMON")
    panLiftLeft = (4096, "PANEL_LIFTLEFT")
    panLiftRight = (8192, "PANEL_LIFTRIGHT")

  MapPanelFlags* {.size: sizeof(uint8), bitsetNullValue: "PANEL_FLAG_NONE".} = enum
    pfBlending = "PANEL_FLAG_BLENDING"
    pfHide = "PANEL_FLAG_HIDE"
    pfWaterTexture = "PANEL_FLAG_WATERTEXTURES"

  MapPanel* = ref object of MapObject
    position* {.writeDefault.}: DFPoint
    size* {.writeDefault.}: DFSize
    texture* {.writeDefault.}: MapTexture
    kind* {.writeDefault.}: MapPanelType
    alpha*: uint8
    flags*: set[MapPanelFlags]
    moveSpeed* {.serializeTextOnly.}: DFPoint
    sizeSpeed* {.serializeTextOnly.}: DFPoint
    moveStart* {.serializeTextOnly.}: DFPoint
    moveEnd* {.serializeTextOnly.}: DFPoint
    sizeEnd* {.serializeTextOnly.}: DFSize
    moveActive* {.serializeTextOnly.}: bool
    moveOnce* {.serializeTextOnly.}: bool
    endPosTrigger* {.serializeTextOnly.}: MapTrigger
    endSizeTrigger*  {.serializeTextOnly.}: MapTrigger
    # editor-specific fields
    id* {.serializeNever, nonEditable, invisible.}: int32

  MapItemType* {.size: sizeof(uint8).} = enum
    itemNone = "ITEM_NONE"
    itemMedkitSmall = "ITEM_MEDKIT_SMALL"
    itemMedkitLarge = "ITEM_MEDKIT_LARGE"
    itemMedkitBlack = "ITEM_MEDKIT_BLACK"
    itemArmorGreen = "ITEM_ARMOR_GREEN"
    itemArmorBlue = "ITEM_ARMOR_BLUE"
    itemSphereBlue = "ITEM_SPHERE_BLUE"
    itemSphereWhite = "ITEM_SPHERE_WHITE"
    itemSuit = "ITEM_SUIT"
    itemOxygen = "ITEM_OXYGEN"
    itemInvul = "ITEM_INVUL"
    itemWeaponSaw = "ITEM_WEAPON_SAW"
    itemWeaponShotgun1 = "ITEM_WEAPON_SHOTGUN"
    itemWeaponShotgun2 = "ITEM_WEAPON_SHOTGUN2"
    itemWeaponChaingun = "ITEM_WEAPON_CHAINGUN"
    itemWeaponRocketLauncher = "ITEM_WEAPON_ROCKETLAUNCHER"
    itemWeaponPlasma = "ITEM_WEAPON_PLASMA"
    itemWeaponBfg = "ITEM_WEAPON_BFG"
    itemWeaponSuperChaingun = "ITEM_WEAPON_SUPERPULEMET"
    itemAmmoBullets = "ITEM_AMMO_BULLETS"
    itemAmmoBulletsBox = "ITEM_AMMO_BULLETS_BOX"
    itemAmmoShells = "ITEM_AMMO_SHELLS"
    itemAmmoShellsBox = "ITEM_AMMO_SHELLS_BOX"
    itemAmmoRocket = "ITEM_AMMO_ROCKET"
    itemAmmoRocketBox = "ITEM_AMMO_ROCKET_BOX"
    itemAmmoCell = "ITEM_AMMO_CELL"
    itemAmmoCellBig = "ITEM_AMMO_CELL_BIG"
    itemAmmoBackpack = "ITEM_AMMO_BACKPACK"
    itemKeyRed = "ITEM_KEY_RED"
    itemKeyGreen = "ITEM_KEY_GREEN"
    itemKeyBlue = "ITEM_KEY_BLUE"
    itemWeaponFist = "ITEM_WEAPON_KASTET"
    itemWeaponPistol = "ITEM_WEAPON_PISTOL"
    itemHealthBonus = "ITEM_BOTTLE"
    itemArmorBonus = "ITEM_HELMET"
    itemJetpack = "ITEM_JETPACK"
    itemInvis = "ITEM_INVIS"
    itemWeaponFlamethrower = "ITEM_WEAPON_FLAMETHROWER"
    itemAmmoFuelCan = "ITEM_AMMO_FUELCAN"

  MapItemFlags* {.size: sizeof(uint8), bitsetNullValue: "ITEM_OPTION_NONE".} = enum
    ifOnlyDM = "ITEM_OPTION_ONLYDM"
    ifFall = "ITEM_OPTION_FALL"

  MapItem* = ref object of MapObject
    position* {.writeDefault.}: DFPoint
    kind* {.writeDefault.}: MapItemType
    options*: set[MapItemFlags]
    # editor-specific fields
    id* {.serializeNever, nonEditable, invisible.}: int32
    size* {.serializeNever, nonEditable.}: DFSize
    tex* {.serializeNever, nonEditable, invisible.}: Texture

  MapMonsterType* {.size: sizeof(uint8).} = enum
    monNone = "MONSTER_NONE"
    monDemon = "MONSTER_DEMON"
    monImp = "MONSTER_IMP"
    monZombie = "MONSTER_ZOMBIE"
    monSergeant = "MONSTER_SERG"
    monCyber = "MONSTER_CYBER"
    monChaingunner = "MONSTER_CGUN"
    monBaron = "MONSTER_BARON"
    monKnight = "MONSTER_KNIGHT"
    monCaco = "MONSTER_CACO"
    monLostSoul = "MONSTER_SOUL"
    monPain = "MONSTER_PAIN"
    monSpider = "MONSTER_SPIDER"
    monBigSpider = "MONSTER_BSP"
    monMancubus = "MONSTER_MANCUB"
    monRevenant = "MONSTER_SKEL"
    monArchVile = "MONSTER_VILE"
    monFish = "MONSTER_FISH"
    monBarrel = "MONSTER_BARREL"
    monRobo = "MONSTER_ROBO"
    monPrikolist = "MONSTER_MAN"

  MapMonsterBehaviour* {.size: sizeof(uint8).} = enum
    behNormal = "BH_NORMAL"
    behKiller = "BH_KILLER"
    behManiac = "BH_MANIAC"
    behInsane = "BH_INSANE"
    behCannibal = "BH_CANNIBAL"
    behGood = "BH_GOOD"

  MapMonster* = ref object of MapObject
    position* {.writeDefault.}: DFPoint
    kind* {.writeDefault.}: MapMonsterType
    direction*: MapDirection
    # editor-specific fields
    id* {.serializeNever, nonEditable, invisible.}: int32
    size* {.serializeNever, nonEditable.}: DFSize
    center* {.serializeNever, nonEditable, invisible.}: DFPoint
    delta* {.serializeNever, nonEditable, invisible.}: DFPoint
    tex* {.serializeNever, nonEditable, invisible.}: Texture

  MapAreaType* {.size: sizeof(uint8).} = enum
    areaNone = "AREA_NONE"
    areaP1Spawn = "AREA_PLAYERPOINT1"
    areaP2Spawn = "AREA_PLAYERPOINT2"
    areaDMSpawn = "AREA_DMPOINT"
    areaRedFlag = "AREA_REDFLAG"
    areaBlueFlag = "AREA_BLUEFLAG"
    areaDomFlag = "AREA_DOMFLAG"
    areaRedSpawn = "AREA_REDTEAMPOINT"
    areaBlueSpawn = "AREA_BLUETEAMPOINT"

  MapArea* = ref object of MapObject
    position* {.writeDefault.}: DFPoint
    kind* {.writeDefault.}: MapAreaType
    direction*: MapDirection
    # editor-specific fields
    id* {.serializeNever, nonEditable, invisible.}: int32
    size* {.serializeNever, nonEditable.}: DFSize
    center* {.serializeNever, nonEditable, invisible.}: DFPoint
    tex* {.serializeNever, nonEditable, invisible.}: Texture

  MapActivationType* {.size: sizeof(uint8), bitsetNullValue: "ACTIVATE_NONE".} = enum
    actPlayerCollide = "ACTIVATE_PLAYERCOLLIDE"
    actMonsterCollide = "ACTIVATE_MONSTERCOLLIDE"
    actPlayerPress = "ACTIVATE_PLAYERPRESS"
    actMonsterPress = "ACTIVATE_MONSTERPRESS"
    actShot = "ACTIVATE_SHOT"
    actNoMonster = "ACTIVATE_NOMONSTER"

  MapTriggerData* {.triggerData.} = ref object of Serializable

  MapTriggerType* {.size: sizeof(uint8).} = enum
    trigNone = "TRIGGER_NONE"
    trigExit = "TRIGGER_EXIT"
    trigTeleport = "TRIGGER_TELEPORT"
    trigOpenDoor = "TRIGGER_OPENDOOR"
    trigCloseDoor = "TRIGGER_CLOSEDOOR"
    trigDoor = "TRIGGER_DOOR"
    trigDoor5 = "TRIGGER_DOOR5"
    trigCloseTrap = "TRIGGER_CLOSETRAP"
    trigTrap = "TRIGGER_TRAP"
    trigPress = "TRIGGER_PRESS"
    trigSecret = "TRIGGER_SECRET"
    trigLiftUp = "TRIGGER_LIFTUP"
    trigLiftDown = "TRIGGER_LIFTDOWN"
    trigLift = "TRIGGER_LIFT"
    trigTexture = "TRIGGER_TEXTURE"
    trigOn = "TRIGGER_ON"
    trigOff = "TRIGGER_OFF"
    trigOnOff = "TRIGGER_ONOFF"
    trigSound = "TRIGGER_SOUND"
    trigSpawnMonster = "TRIGGER_SPAWNMONSTER"
    trigSpawnItem = "TRIGGER_SPAWNITEM"
    trigMusic = "TRIGGER_MUSIC"
    trigPush = "TRIGGER_PUSH"
    trigScore = "TRIGGER_SCORE"
    trigMessage = "TRIGGER_MESSAGE"
    trigDamage = "TRIGGER_DAMAGE"
    trigHealth = "TRIGGER_HEALTH"
    trigShot = "TRIGGER_SHOT"
    trigEffect = "TRIGGER_EFFECT"
    trigScript = "TRIGGER_SCRIPT"

  MapTrigger* = ref object of MapObject
    position* {.writeDefault.}: DFPoint
    size* {.writeDefault.}: DFSize
    enabled* {.defaultValue: true.}: bool
    texturePanel*: MapPanel
    kind* {.writeDefault.}: MapTriggerType
    activateType* {.writeDefault.}: set[MapActivationType]
    keys*: set[MapKey]
    triggerData* {.writeDefault, serializedBlockName: "triggerdata", triggerData.}: MapTriggerData # must always be last
    # editor-specific fields
    id* {.serializeNever, nonEditable.}: int32

  MapHeader* = ref object of Serializable
    mapName* {.writeDefault, serializedFieldName: "name", maxSize: 32.}: string
    author* {.writeDefault, maxSize: 32.}: string
    description* {.writeDefault, maxSize: 256.}: string
    music* {.writeDefault, resPath, maxSize: 64.}: string
    sky* {.writeDefault, resPath, maxSize: 64.}: string
    size*  {.writeDefault.}: DFSize

  Map* = ref object of Serializable
    header* {.writeDefault, serializedBlockId: 7.}: MapHeader
    textures* {.writeDefault, serializedBlockId: 1, serializedBlockName: "texture".}: ref seq[MapTexture]
    panels* {.writeDefault, serializedBlockId: 2, serializedBlockName: "panel".}: ref seq[MapPanel]
    items* {.writeDefault, serializedBlockId: 3, serializedBlockName: "item".}: ref seq[MapItem]
    monsters* {.writeDefault, serializedBlockId: 5, serializedBlockName: "monster".}: ref seq[MapMonster]
    areas* {.writeDefault, serializedBlockId: 4, serializedBlockName: "area".}: ref seq[MapArea]
    triggers* {.writeDefault, serializedBlockId: 6, serializedBlockName: "trigger".}: ref seq[MapTrigger]

  SomeMapObject* = MapPanel | MapItem | MapMonster | MapArea | MapTrigger
  SomeMapRecord* = SomeMapObject | MapTexture

  MapLayer* = enum
    lrBackground = "Background"
    lrWalls = "Walls"
    lrForeground = "Foreground"
    lrSteps = "Steps"
    lrLiquids = "Liquids"
    lrItems = "Items"
    lrMonsters = "Monsters"
    lrAreas = "Areas"
    lrTriggers = "Triggers"

include helpers

# TRIGGER DATA TYPES START #

triggerDataDefs:
  triggerDataType({ trigExit }):
    type MapTriggerDataExit* = ref object of MapTriggerData
      map* {.writeDefault, maxSize: 16.}: string

  triggerDataType({ trigTeleport }):
    type MapTriggerDataTeleport* = ref object of MapTriggerData
      target* {.writeDefault.}: DFPoint
      d2d*: bool
      silent*: bool
      direction*: MapDirection

  triggerDataType({ trigOpenDoor..trigTrap, trigLiftUp..trigLift }):
    type MapTriggerDataPanel* = ref object of MapTriggerData
      panel* {.writeDefault, serializedFieldName: "panelid".}: MapPanel
      silent*: bool
      d2d*: bool

  triggerDataType({ trigPress, trigOn..trigOnOff }):
    type MapTriggerDataPress* = ref object of MapTriggerData
      position*: DFPoint
      size*: DFSize
      wait*: uint16
      count*: uint16
      monster* {.serializedFieldName: "monsterid".}: MapMonster
      extRandom*: bool
      # extended fields
      movePanel* {.serializeTextOnly, serializedFieldName: "panelid".}: MapPanel
      silent* {.serializeTextOnly, defaultValue: true.}: bool
      sound* {.serializeTextOnly.}: string

  triggerDataType({ trigTexture }):
    type MapTriggerDataTexture* = ref object of MapTriggerData
      activateOnce* {.writeDefault.}: bool
      animateOnce* {.writeDefault.}: bool

  triggerDataType({ trigSound }):
    type MapTriggerDataSound* = ref object of MapTriggerData
      soundName* {.writeDefault, resPath, maxSize: 64.}: string
      volume* {.writeDefault.}: uint8
      pan*: uint8
      local* {.defaultValue: true.}: bool
      playCount*: uint8
      soundSwitch*: bool

  triggerDataType({ trigSpawnMonster }):
    type MapTriggerDataSpawnMonster* = ref object of MapTriggerData
      position* {.writeDefault.}: DFPoint
      kind* {.writeDefault, overrideSize: sizeof(uint32).}: MapMonsterType
      health* {.writeDefault.}: int32
      direction* {.writeDefault.}: MapDirection
      active* {.writeDefault, overrideSize: 3, defaultValue: true.}: bool
      count* {.writeDefault, defaultValue: 1.}: int32
      effect* {.writeDefault.}: MapSpawnEffect
      max* {.writeDefault, defaultValue: 1.}: uint16
      delay* {.writeDefault, defaultValue: 1000.}: uint16
      behaviour*: MapMonsterBehaviour

  triggerDataType({ trigSpawnItem }):
    type MapTriggerDataSpawnItem* = ref object of MapTriggerData
      position* {.writeDefault.}: DFPoint
      kind* {.writeDefault.}: MapItemType
      gravity* {.defaultValue: true.}: bool
      dmonly* {.overrideSize: 2.}: bool
      count* {.defaultValue: 1.}: int32
      effect* {.overrideSize: 2.}: MapSpawnEffect
      max* {.defaultValue: 1.}: uint16
      delay* {.defaultValue: 1.}: uint16

  type
    MapTriggerMusicAction* {.size: sizeof(uint8).} = enum
      musActionStop = "TRIGGER_MUSIC_ACTION_STOP"
      musActionPlay = "TRIGGER_MUSIC_ACTION_PLAY"

  triggerDataType({ trigMusic }):
    type MapTriggerDataMusic* = ref object of MapTriggerData
      musicName* {.writeDefault, serializedFieldName: "name", resPath, maxSize: 64.}: string
      action* {.writeDefault.}: MapTriggerMusicAction

  triggerDataType({ trigPush }):
    type MapTriggerDataPush* = ref object of MapTriggerData
      angle* {.writeDefault.}: uint16
      force* {.writeDefault.}: uint8
      resetVelocity* {.writeDefault.}: bool

  type
    MapTriggerScoreAction* {.size: sizeof(uint8).} = enum
      scoreActionAdd = "TRIGGER_SCORE_ACTION_ADD"
      scoreActionSub = "TRIGGER_SCORE_ACTION_SUB"
      scoreActionWin = "TRIGGER_SCORE_ACTION_WIN"
      scoreActionLose = "TRIGGER_SCORE_ACTION_LOOSE" # sic

    MapTriggerScoreTeam* {.size: sizeof(uint8).} = enum
      scoreTeamMineRed = "TRIGGER_SCORE_TEAM_MINE_RED"
      scoreTeamMineBlue = "TRIGGER_SCORE_TEAM_MINE_BLUE"
      scoreTeamForceRed = "TRIGGER_SCORE_TEAM_FORCE_RED"
      scoreTeamForceBlue = "TRIGGER_SCORE_TEAM_FORCE_BLUE"

  triggerDataType({ trigScore }):
    type MapTriggerDataScore* = ref object of MapTriggerData
      action* {.writeDefault.}: MapTriggerScoreAction
      count* {.writeDefault, defaultValue: 1.}: uint8
      team* {.writeDefault.}: MapTriggerScoreTeam
      console* {.writeDefault.}: bool
      message* {.writeDefault, defaultValue: true.}: bool

  type
    MapTriggerMessageKind* {.size: sizeof(uint8).} = enum
      msgKindChat = "TRIGGER_MESSAGE_KIND_CHAT"
      msgKindGame = "TRIGGER_MESSAGE_KIND_GAME"

    MapTriggerMessageDest* {.size: sizeof(uint8).} = enum
      msgDestMe = "TRIGGER_MESSAGE_DEST_ME"
      msgDestMyTeam = "TRIGGER_MESSAGE_DEST_MY_TEAM"
      msgDestEnemyTeam = "TRIGGER_MESSAGE_DEST_ENEMY_TEAM"
      msgDestRedTeam = "TRIGGER_MESSAGE_DEST_RED_TEAM"
      msgDestBlueTeam = "TRIGGER_MESSAGE_DEST_BLUE_TEAM"
      msgDestEveryone = "TRIGGER_MESSAGE_DEST_EVERYONE"

  triggerDataType({ trigMessage }):
    type MapTriggerDataMessage* = ref object of MapTriggerData
      kind* {.writeDefault, serializedFieldName: "kind".}: MapTriggerMessageKind
      dest* {.writeDefault.}: MapTriggerMessageDest
      text* {.writeDefault, maxSize: 100.}: string
      time* {.writeDefault.}: uint16

  triggerDataType({ trigDamage }):
    type MapTriggerDataDamage* = ref object of MapTriggerData
      amount* {.writeDefault.}: uint16
      interval* {.writeDefault.}: uint16
      kind* {.writeDefault, serializedFieldName: "kind".}: MapHitType

  triggerDataType({ trigHealth }):
    type MapTriggerDataHealth* = ref object of MapTriggerData
      amount* {.writeDefault.}: uint16
      interval* {.writeDefault.}: uint16
      max* {.writeDefault.}: bool
      silent* {.writeDefault.}: bool

  type
    MapTriggerShotKind* {.size: sizeof(uint8).} = enum
      shotKindPistol = "TRIGGER_SHOT_PISTOL"
      shotKindBullet = "TRIGGER_SHOT_BULLET"
      shotKindShotgun = "TRIGGER_SHOT_SHOTGUN"
      shotKindSSG = "TRIGGER_SHOT_SSG"
      shotKindImp = "TRIGGER_SHOT_IMP"
      shotKindPlasma = "TRIGGER_SHOT_PLASMA"
      shotKindSpider = "TRIGGER_SHOT_SPIDER"
      shotKindCaco = "TRIGGER_SHOT_CACO"
      shotKindBaron = "TRIGGER_SHOT_BARON"
      shotKindMancub = "TRIGGER_SHOT_MANCUB"
      shotKindRev = "TRIGGER_SHOT_REV"
      shotKindRocket = "TRIGGER_SHOT_ROCKET"
      shotKindBFG = "TRIGGER_SHOT_BFG"
      shotKindExpl = "TRIGGER_SHOT_EXPL"
      shotKindBFGExpl = "TRIGGER_SHOT_BFGEXPL"
      shotKindFlame = "TRIGGER_SHOT_FLAME"
    MapTriggerShotTarget* {.size: sizeof(uint8).} = enum
      shotTargetNone = "TRIGGER_SHOT_TARGET_NONE"
      shotTargetMon = "TRIGGER_SHOT_TARGET_MON"
      shotTargetPlr = "TRIGGER_SHOT_TARGET_PLR"
      shotTargetRed = "TRIGGER_SHOT_TARGET_RED"
      shotTargetBlue = "TRIGGER_SHOT_TARGET_BLUE"
      shotTargetMonPlr = "TRIGGER_SHOT_TARGET_MONPLR"
      shotTargetPlrMon = "TRIGGER_SHOT_TARGET_PLRMON"
    MapTriggerShotAim* {.size: sizeof(uint8).} = enum
      shotAimDefault = "TRIGGER_SHOT_AIM_DEFAULT"
      shotAimAllMap = "TRIGGER_SHOT_AIM_ALLMAP"
      shotAimTrace = "TRIGGER_SHOT_AIM_TRACE"
      shotAimTraceAll = "TRIGGER_SHOT_AIM_TRACEALL"

  triggerDataType({ trigShot }):
    type MapTriggerDataShot* = ref object of MapTriggerData
      position* {.writeDefault.}: DFPoint
      kind* {.writeDefault.}: MapTriggerShotKind
      target* {.writeDefault.}: MapTriggerShotTarget
      sound*: bool # negbool?
      aim*: MapTriggerShotAim
      panel* {.writeDefault, serializedFieldName: "panelid".}: MapPanel
      sight*: uint16
      angle*: uint16
      wait*: uint16
      accuracy*: uint16
      ammo*: uint16
      reload*: uint16

  type
    MapTriggerEffectType* {.size: sizeof(uint8).} = enum
      effectTypeParticle = "TRIGGER_EFFECT_PARTICLE"
      effectTypeAnimation = "TRIGGER_EFFECT_ANIMATION"
    MapTriggerEffectSubType* {.size: sizeof(uint8).} = enum
      effectSubTypeSLiquid = "TRIGGER_EFFECT_SLIQUID"
      effectSubTypeLLiquid = "TRIGGER_EFFECT_LLIQUID"
      effectSubTypeDLiquid = "TRIGGER_EFFECT_DLIQUID"
      effectSubTypeBlood = "TRIGGER_EFFECT_BLOOD"
      effectSubTypeSpark = "TRIGGER_EFFECT_SPARK"
      effectSubTypeBubble = "TRIGGER_EFFECT_BUBBLE"
    MapTriggerEffectPos* {.size: sizeof(uint8).} = enum
      effectPosCenter = "TRIGGER_EFFECT_POS_CENTER"
      effectPosArea = "TRIGGER_EFFECT_POS_AREA"

  triggerDataType({ trigEffect }):
    type MapTriggerDataEffect* = ref object of MapTriggerData
      count* {.writeDefault.}: uint8
      kind* {.writeDefault.}: MapTriggerEffectType
      subType* {.writeDefault.}: MapTriggerEffectSubType
      red* {.writeDefault.}: uint8
      green* {.writeDefault.}: uint8
      blue* {.writeDefault.}: uint8
      pos* {.writeDefault.}: MapTriggerEffectPos
      wait* {.writeDefault.}: uint16
      velX* {.writeDefault.}: int8
      velY* {.writeDefault.}: int8
      spreadL* {.writeDefault.}: uint8
      spreadR* {.writeDefault.}: uint8
      spreadU* {.writeDefault.}: uint8
      spreadD* {.writeDefault.}: uint8

  triggerDataType({ trigSecret }):
    type MapTriggerDataSecret* = ref object of MapTriggerData

# TRIGGER DATA TYPES END #

var
  currentMap*: Map = nil

# intiializers for map structs, executed after loading

proc init*(self: MapPanel) =
  discard

proc init*(self: MapTrigger) =
  discard

proc init*(self: MapTexture) =
  self.tex = gfx.getTexture(self.path)
  if self.tex == nil:
    self.tex = gfx.getTexture("$$ERROR")

proc init*(self: MapItem) =
  let typeStr = $self.kind
  if typeStr in mapEntInfo:
    let (_, _, size, _, _) = mapEntInfo[typeStr]
    self.tex = gfx.getTexture("$$" & typeStr)
    self.size = size

proc init*(self: MapArea) =
  let typeStr = $self.kind
  if typeStr in mapEntInfo:
    let (center, _, size, _, _) = mapEntInfo[typeStr]
    self.tex = gfx.getTexture("$$" & typeStr)
    self.size = size
    self.center = center

proc init*(self: MapMonster) =
  let typeStr = $self.kind
  if typeStr in mapEntInfo:
    let (center, delta, size, _, _) = mapEntInfo[typeStr]
    self.tex = gfx.getTexture("$$" & typeStr)
    self.size = size
    self.center = center
    self.delta = delta

# map-wide stuff

iterator mapAllOfType*[T: SomeMapRecord](): var T =
  # uh oh stinky
  when T is MapTexture:
    for x in currentMap.textures[].mitems(): yield x
  elif T is MapPanel:
    for x in currentMap.panels[].mitems(): yield x
  elif T is MapItem:
    for x in currentMap.items[].mitems(): yield x
  elif T is MapMonster:
    for x in currentMap.monsters[].mitems(): yield x
  elif T is MapArea:
    for x in currentMap.areas[].mitems(): yield x
  elif T is MapTrigger:
    for x in currentMap.triggers[].mitems(): yield x
  else:
    error("this shouldn't happen")

proc mapGetAllOfType*[T: SomeMapRecord](): seq[T] =
  # uh oh stinky
  when T is MapTexture:
    return currentMap.textures
  elif T is MapPanel:
    return currentMap.panels
  elif T is MapItem:
    return currentMap.items
  elif T is MapMonster:
    return currentMap.monsters
  elif T is MapArea:
    return currentMap.areas
  elif T is MapTrigger:
    return currentMap.triggers
  else:
    error("this shouldn't happen")

template mapForAllRecords*(obj, blk: untyped): untyped =
  # uh oh stinky
  for obj in mapAllOfType[MapTexture](): blk
  for obj in mapAllOfType[MapPanel](): blk
  for obj in mapAllOfType[MapItem](): blk
  for obj in mapAllOfType[MapMonster](): blk
  for obj in mapAllOfType[MapArea](): blk
  for obj in mapAllOfType[MapTrigger](): blk

template mapForAllObjects*(obj, blk: untyped): untyped =
  # uh oh stinky
  for obj in mapAllOfType[MapPanel](): blk
  for obj in mapAllOfType[MapItem](): blk
  for obj in mapAllOfType[MapMonster](): blk
  for obj in mapAllOfType[MapArea](): blk
  for obj in mapAllOfType[MapTrigger](): blk

proc mapGetByName*[T: SomeMapRecord](name: string): T =
  if name != "":
    for x in mapAllOfType[T]():
      if name == x.name:
        return x
  result = nil

proc mapGetById*[T: SomeMapRecord](id: int): T =
  if id >= 0 and id != 0xFFFF:
    for x in mapAllOfType[T]():
      if id == x.id.int:
        return x
  result = nil

proc newMapHeader*(name: string, w, h: int): MapHeader =
  result = new(MapHeader)
  result.mapName = name
  result.size = initDFSize(w, h)

proc newMap*(): Map =
  result = new(Map)
  result.header = newMapHeader("Unnamed Map", 1600, 1600)
  new(result.textures)
  new(result.panels)
  new(result.items)
  new(result.monsters)
  new(result.areas)
  new(result.triggers)

proc newMap*(stream: SomeSerializer): Map =
  assert(not stream.isWriting, "can't create map from writer stream")
  result = newMap()
  stream.serialize(result)
  stream.finalize()

proc save*(self: Map, stream: SomeSerializer) =
  assert(stream.isWriting, "can't save map to reader stream")
  stream.serialize(self)
  stream.finalize()

proc destroy*(self: Map) =
  for mtex in self.textures[].mitems():
    if mtex.path notin waterTextureNames:
      mtex.tex.destroy()
  self.textures[].setLen(0)
  self.panels[].setLen(0)
  self.items[].setLen(0)
  self.monsters[].setLen(0)
  self.areas[].setLen(0)
  self.triggers[].setLen(0)

proc referenceScan[T: Serializable](self: Map, x: T) =
  for fieldName, fieldSym in fieldPairs(x[]):
    when fieldSym is SomeMapRecord:
      if fieldSym != nil:
        when T is MapPanel and fieldSym is MapTexture:
          if pfHide in x.flags:
            fieldSym = nil # hidden panel, no texture
        if fieldSym != nil:
          let searchName = fieldSym.name
          let searchId = fieldSym.id
          fieldSym = mapGetById[type(fieldSym)](searchId.int)
          if fieldSym == nil:
            fieldSym = mapGetByName[type(fieldSym)](searchName)
    elif fieldSym is MapTriggerData:
      fieldSym.fixupReferences(self)

proc fixupReferences*(self: Map) =
  mapForAllObjects(x):
    self.referenceScan(x)
