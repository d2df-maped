# trigger data always has the same size
proc size*(self: MapTriggerData): int = 128

# map objects have sizes of their binary representation
proc size*(self: SomeMapRecord): int = self.binarySize()

# references to map objects are serialized as names or indices
proc serializeId*(stream: SomeSerializer, name: string, value: SomeMapRecord) =
  if not stream.isWriting:
    # HACK: turn self into a placeholder, it'll get fixed up later
    value.id = cast[value.id.type](-1)
    value.name = ""
    value.placeholder = true
  when stream is BinSerializer:
    if stream.isWriting:
      var id: type(value.id)
      if value != nil:
        id = value.id
        # monster IDs start with 1 in the map file
        when value is MapMonster: id.inc()
      else:
        # texture references have 0 as the default id even though it's a valid texture id
        # monster IDs start with 1 in the map file
        when value is MapTexture or value is MapMonster:
          id = default(value.id.type)
        else:
          id = cast[value.id.type](-1)
      stream.serialize(name, id)
    else:
      stream.serialize(name, value.id)
      # monster IDs start with 1 in the map file but with 0 in the editor
      when value is MapMonster: value.id.dec()
  else:
    if stream.isWriting:
      var tmp = (if value != nil: value.name else: "null")
      stream.serialize(name, tmp)
    else:
      stream.serialize(name, value.name)

proc referenceScan*[T: Serializable](self: Map, x: T) {.locks: 0.}

method fixupReferences(self: MapTriggerData, map: Map) {.base.} =
  map.referenceScan(self)

# all trigger data subtypes have to be declared with this, it'll generate procs and other shit
macro triggerDataType(forTypes: static[set[MapTriggerType]], decl: untyped): untyped =
  # get name of the subtype
  let identType = decl[0][0][0][1] # TypeSection { TypeDef { Postfix { *, tdType } } }
  # form everything into one statement list
  result = newStmtList()
  result.add(decl) # add the type decl first
  result.add newProc(
    postfix(ident("serializeDynamic"), "*"),
    @[newEmptyNode(), newIdentDefs(ident("self"), identType), newIdentDefs(ident("stream"), ident("BinSerializer"))],
    newStmtList(newCall(ident("serialize"), ident("stream"), ident("self"))),
    nnkMethodDef
  )
  result.add newProc(
    postfix(ident("serializeDynamic"), "*"),
    @[newEmptyNode(), newIdentDefs(ident("self"), identType), newIdentDefs(ident("stream"), ident("TextSerializer"))],
    newStmtList(newCall(ident("serialize"), ident("stream"), ident("self"))),
    nnkMethodDef
  )
  result.add newProc(
    ident("fixupReferences"),
    @[newEmptyNode(), newIdentDefs(ident("self"), identType), newIdentDefs(ident("map"), ident("Map"))],
    newStmtList(newCall(ident("referenceScan"), ident("map"), ident("self"))),
    nnkMethodDef
  )

# this will collect all the trigger data type decls and emit conversion procs
macro triggerDataDefs(declList: untyped): untyped =
  result = declList
  # collect all the types into a big fuck you switch-case that'll give you the correct type
  var caseStmt = newNimNode(nnkCaseStmt)
  caseStmt.add(newDotExpr(ident("self"), ident("kind")))
  for macroCall in declList:
    if macroCall.kind == nnkCall and macroCall[0] == ident("triggerDataType"):
      let enumSet = macroCall[1] # Curly
      let typeName = macroCall[2][0][0][0][1] # Call { TypeSection { TypeDef { Postfix { *, tdType } } } }
      var branch = newNimNode(nnkOfBranch)
      for e in enumSet:
        branch.add(e)
      branch.add(newStmtList(newCall("new", typeName)))
      caseStmt.add(branch)
  var elseBranch = newNimNode(nnkElse)
  elseBranch.add(newStmtList(newCall("new", ident("MapTriggerData"))))
  caseStmt.add(elseBranch)
  # emit constructor
  result.add newProc(
    postfix(ident("newTriggerData"), "*"),
    @[ident("MapTriggerData"), newIdentDefs(ident("self"), ident("MapTrigger"))],
    newStmtList(caseStmt),
  )
