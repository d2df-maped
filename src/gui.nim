import
  std/math,
  thirdparty/imgui,
  utils, gfx, map, vfs, config,
  gui/[common, controls, file_dialog, wad_dialog, map_dialog, message_box]

export
  common, controls, file_dialog, wad_dialog, map_dialog, message_box, imgui

const
  customBarWidth* = 8
  menuBarHeight* = 20

var
  customBarColBg*: DFColor = colDkGrey
  customBarColFg*: DFColor = colGrey
  customBarColMove*: DFColor = colWhite
  dockSizeLeft* = 0
  dockSizeRight* = 150
  dockSizeBottom* = 150
  selection*: seq[MapObject] = @[]

type
  CustomScrollBar* = object
    axis: int # 0=x, 1=y
    start: DFPoint
    size: DFSize
    barSize: DFSize
    barStart: DFPoint
    maxSize: int
    active: bool

type
  ActionProc = proc (): void

var
  confirmBox: MessageBox
  confirmAction: ActionProc = nil

proc toDFColor*(c: ImVec4): DFColor =
  result.r = uint8(c.x * 255f)
  result.g = uint8(c.y * 255f)
  result.b = uint8(c.z * 255f)
  result.a = uint8(c.w * 255f)

proc shutdown*() =
  discard

proc init*() =
  addExitProc(gui.shutdown)
  let style = imgui.getStyle()
  customBarColBg = style.colors[ImGuiCol.ScrollbarBg.int32].toDFColor()
  customBarColBg.a = 0xFF'u8
  customBarColFg = style.colors[ImGuiCol.ScrollbarGrab.int32].toDFColor()
  customBarColMove = style.colors[ImGuiCol.ScrollbarGrabActive.int32].toDFColor()

proc openDiscardWarning() =
  confirmBox.open(mbQuestion, "Are you sure you want to discard the current map?")

proc emitMapOpenDialog() =
  defaultFileDialog.open("Select WAD", "maps", archiveExts, false, "")

proc emitMapSelectDialog() =
  discard

proc emitMainFileMenu() =
  if imgui.menuItem("New Map", "Ctrl+N"):
    openDiscardWarning()
    confirmAction = map.create
  if imgui.menuItem("Open Map", "Ctrl+O"):
    openDiscardWarning()
    confirmAction = emitMapOpenDialog
  if imgui.menuItem("Select Map"):
    openDiscardWarning()
    confirmAction = emitMapSelectDialog
  if imgui.menuItem("Reopen Map", "F5"):
    openDiscardWarning()
    confirmAction = map.reopen
  imgui.separator()
  if imgui.menuItem("Save Map", "Ctrl+S"):
    discard
  if imgui.menuItem("Save Map As..."):
    discard
  if imgui.menuItem("Pack Map"):
    discard
  if imgui.menuItem("Delete Map from WAD..."):
    discard
  imgui.separator()
  if imgui.menuItem("Exit", "F10"):
    confirmBox.open(mbQuestion, "Are you sure you want to quit?")
    confirmAction = nil

proc emitMainEditMenu() =
  if imgui.menuItem("Undo", "Ctrl+Z"):
    discard
  if imgui.menuItem("Redo", "Ctrl+Y"):
    discard
  imgui.separator()
  if imgui.menuItem("Copy", "Ctrl+C"):
    discard
  if imgui.menuItem("Cut", "Ctrl+X"):
    discard
  if imgui.menuItem("Paste", "Ctrl+V"):
    discard
  imgui.separator()
  if imgui.menuItem("Select All", "Ctrl+A"):
    discard
  if imgui.menuItem("Invert Selection"):
    discard
  imgui.separator()
  if imgui.menuItem("Snap to Grid", "Ctrl+Q"):
    discard
  if imgui.menuItem("Bring to Front", "Ctrl+]"):
    discard
  if imgui.menuItem("Send to Back", "Ctrl+["):
    discard

proc emitMainViewMenu() =
  imgui.doMenu("Layers"):
    for i, x in config.showLayer.mpairs():
      imgui.menuItem($i, $(i.int + 1), x.addr)
  imgui.separator()
  imgui.menuItem("Show Map edges", "", config.showMapEdges.addr)
  imgui.menuItem("Show Grid", "", config.showGrid.addr)
  imgui.menuItem("Show Mini Map", "", config.showMiniMap.addr)
  imgui.separator()
  imgui.menuItem("Preview Mode", "TAB", config.showPreview.addr)

proc emitMainPropertiesMenu() =
  if imgui.menuItem("Switch Grid step", "Ctrl+E"):
    discard
  imgui.separator()
  if imgui.menuItem("Map Properties"):
    discard
  imgui.separator()
  if imgui.menuItem("Editor Settings"):
    discard

proc emitMainToolsMenu() =
  if imgui.menuItem("Check Map"):
    discard
  if imgui.menuItem("Optimize Map"):
    discard
  if imgui.menuItem("Run Map"):
    discard

proc emitAboutWindow() =
  discard

proc emitMainMenuBar() =
  imgui.doMainMenuBar():
    imgui.doMenu("File"):
      emitMainFileMenu()
    imgui.doMenu("Edit"):
      emitMainEditMenu()
    imgui.doMenu("View"):
      emitMainViewMenu()
    imgui.doMenu("Properties"):
      emitMainPropertiesMenu()
    imgui.doMenu("Tools"):
      emitMainToolsMenu()
    imgui.doMenu("Help"):
      if imgui.menuItem("About"):
        emitAboutWindow()

proc emitSideBar() =
  imgui.setNextWindowPos(ImVec2(x: float32(gfx.vidWidth() - dockSizeRight), y: menuBarHeight.float32))
  imgui.setNextWindowSize(ImVec2(x: dockSizeRight.float32, y: float32(gfx.vidHeight() - menuBarHeight)))
  if imgui.beginWindow("##ObjInfo", nil, ImGuiWindowFlags.NoTitleBar or ImGuiWindowFlags.NoMove or ImGuiWindowFlags.NoResize):
    imgui.endWindow()

proc emitBottomBar() =
  imgui.setNextWindowPos(ImVec2(x: 0f, y: float32(gfx.vidHeight() - dockSizeBottom)))
  imgui.setNextWindowSize(ImVec2(x: (gfx.vidWidth() - dockSizeRight).float32, y: dockSizeBottom.float32))
  if imgui.beginWindow("##BottomBar", nil, ImGuiWindowFlags.NoTitleBar or ImGuiWindowFlags.NoMove or ImGuiWindowFlags.NoResize):
    imgui.endWindow()

proc mainWindow*(): bool =
  result = true

  discard defaultInfoBox.display()
  discard defaultWarningBox.display()
  discard defaultErrorBox.display()

  emitMainMenuBar()
  emitSideBar()
  emitBottomBar()

  if confirmBox.display():
    if confirmBox.getResult() == 1:
      if confirmAction != nil:
        confirmAction()
        confirmAction = nil
      else:
        result = false

  if defaultFileDialog.display():
    let wadPath = defaultFileDialog.getResultPath()
    if wadPath != "":
      if defaultFileDialog.isReading():
        map.close()
        try:
          var ar = vfs.mount(wadPath, true, true, true)
          if ar != nil:
            defaultMapDialog.open("Select map", ar, false)
        except IOError:
          discard defaultErrorBox.open(mbError, getCurrentExceptionMsg())

  if defaultMapDialog.display():
    let mapName = defaultMapDialog.getResult()
    if mapName != "":
      if defaultMapDialog.isReading():
        map.close()
        map.select(mapName)

proc maxViewportSize*(): DFSize =
  result = gfx.vidSize()
  result.w -= dockSizeLeft + dockSizeRight + customBarWidth
  result.h -= dockSizeBottom + customBarWidth + menuBarHeight

proc viewportPos*(): DFPoint =
  result = (dockSizeLeft, menuBarHeight)

proc viewportMousePos*(): DFPoint =
  let v = imgui.getMousePos()
  if v.x > -1e10f:
    result.x = v.x.int
  if v.y > -1e10f:
    result.y = v.y.int
  result = result - viewportPos()

proc scrollControls(self: var CustomScrollBar, x: var int) =
  let pos = viewportMousePos()

  if not imgui.isAnyModalPopupOpen():
    if self.active:
      if not imgui.isMouseDown(ImGuiMouseButton.Left):
        self.active = false
    else:
      if imgui.igIsMouseClicked(ImGuiMouseButton.Left):    
        if intersect(pos, self.start, self.size):
          self.active = true
  else:
    self.active = false

  if self.active:
    let mxPos = (if self.axis == 0: pos.x else: pos.y).float32
    let endX = (self.maxSize - self.size.component(self.axis)).float32
    let mxStart = self.barSize.component(self.axis).float32 / 2f
    let mxEnd = self.size.component(self.axis).float32 - mxStart
    let mxRatio = (mxPos - mxStart) / (mxEnd - mxStart)
    x = int(clamp(mxRatio, 0f, 1f) * endX)

proc hscroll*(self: var CustomScrollBar, x: var int, size, maxSize, startY: int) =
  self.axis = 0
  self.start = (0, startY)
  self.size = (size, customBarWidth)
  self.barSize = (int((size.float32 / maxSize.float32) * size.float32), customBarWidth)
  self.barStart = (int((x.float32 / maxSize.float32) * size.float32), self.start.y)
  self.maxSize = maxSize

  self.scrollControls(x)

  gfx.drawQuad(self.start, (self.size.x + customBarWidth, self.size.y), customBarColBg)
  gfx.drawQuad(self.barStart, self.barSize, (if self.active: customBarColMove else: customBarColFg))

proc vscroll*(self: var CustomScrollBar, x: var int, size, maxSize, startX: int) =
  self.axis = 1
  self.start = (startX, 0)
  self.size = (customBarWidth, size)
  self.barSize = (customBarWidth, int((size.float32 / maxSize.float32) * size.float32))
  self.barStart = (self.start.x, int((x.float32 / maxSize.float32) * size.float32))
  self.maxSize = maxSize

  self.scrollControls(x)

  gfx.drawQuad(self.start, (self.size.x, self.size.y + customBarWidth), customBarColBg)
  gfx.drawQuad(self.barStart, self.barSize, (if self.active: customBarColMove else: customBarColFg))

