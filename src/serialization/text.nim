import
  std/streams, std/strutils, std/deques, std/parseutils, std/tables,
  common, ../utils

type
  TextError* = object of SerializationException
  TextReadError* = object of TextError
  TextWriteError* = object of TextError
  TextSerializer* = ref object of Serializer
    stream: Stream
    indentLevel: int
    scopes: Deque[bool] # each bool indicates whether we should actually close the block
    line: int
    curBlockType: string
    curBlock: Table[string, string] # stores fields of the current block while reading

const
  Punctuation = { '.', ',', '/', '\\', '[', ']', '{', '}', '(', ')', '?', '!', '=', '$', '#', '\'', '"', ';', ':' }

proc raiseReadError*(self: TextSerializer, msg: string) =
  self.isValid = false
  raise newException(TextReadError, "error: $1 (line $2): $3" % [self.name, $self.line, msg])

proc raiseWriteError*(self: TextSerializer, msg: string) =
  self.isValid = false
  raise newException(TextWriteError, "error: $1 (line $2): $3" % [self.name, $self.line, msg])

proc raiseError*(self: TextSerializer, msg: string) =
  if self.isWriting:
    self.raiseWriteError(msg)
  else:
    self.raiseReadError(msg)

proc writeIndented(self: TextSerializer, str: varargs[string, `$`]) =
  self.stream.write(spaces(self.indentLevel * 2))
  for s in str.items():
    self.stream.write(s)

proc writeIndentedLn(self: TextSerializer, str: varargs[string, `$`]) =
  self.writeIndented(str)
  self.stream.write("\n")
  self.line.inc()

proc writeLn(self: TextSerializer, str: varargs[string, `$`]) =
  self.stream.writeLine(str)
  self.line.inc()

proc write(self: TextSerializer, str: varargs[string, `$`]) =
  self.stream.write(str)

proc beginMap(self: TextSerializer)

proc newTextSerializer*(name: string, s: Stream, writing: bool): TextSerializer =
  result = new(TextSerializer)
  result.name = name
  result.stream = s
  result.isValid = true
  result.isWriting = writing
  result.line = 1
  result.curBlock = initTable[string, string]()
  result.curBlockType = ""
  result.beginMap()

proc consumeGarbage(self: TextSerializer) =
  while true:
    let ch = self.stream.peekChar()
    if ch in Whitespace:
      discard self.stream.readChar() # discard whitespace
      if ch == '\n': self.line.inc()
    elif ch == '/' and self.stream.peekChar() == '/':
      discard self.stream.readLine() # comment line; discard until end of line
      self.line.inc()
    else:
      break  

proc consumeToken(self: TextSerializer, acceptPunct: bool = true): string =
  result = ""
  self.consumeGarbage()
  while true:
    var ch = self.stream.peekChar()
    if ch in Punctuation:
      if acceptPunct:
        # punctuation is single char only in our format
        result &= self.stream.readChar()
        break
      else:
        # don't want punctuation
        self.raiseReadError("expected identifier, got '" & ch & "'")
    elif ch in IdentChars:
      # looks like a number or a word, read until the end of it
      while ch in IdentChars:
        result &= self.stream.readChar()
        ch = self.stream.peekChar()
      break
    else:
      # unknown garbage
      self.raiseReadError("expected token, got '" & ch & "'")

proc peekToken(self: TextSerializer): string =
  # FIXME: this is bad and not very fast
  let oldPos = self.stream.getPosition()
  let oldLine = self.line
  result = self.consumeToken()
  self.stream.setPosition(oldPos)
  self.line = oldLine

proc consumeUntil(self: TextSerializer, until: set[char], skipSep: bool = true, skipGarbage: bool = true): string =
  result = ""
  if skipGarbage:
    self.consumeGarbage()
  while true:
    let ch = self.stream.peekChar()
    if ch in until:
      if skipSep: discard self.stream.readChar()
      break
    elif ch in Newlines:
      self.raiseReadError("reached end of line looking for one of " & $until)
    else:
      result &= self.stream.readChar()

proc expect(self: TextSerializer, chars: set[char], skipSep: bool = true, skipGarbage: bool = true) =
  if skipGarbage:
    self.consumeGarbage()
  let ch = self.stream.peekChar()
  if not (ch in chars):
    self.raiseReadError("expected one of " & $chars & ", got " & ch)
  if skipSep:
    discard self.stream.readChar()

proc consumeBlockStart(self: TextSerializer, blockType: string, blockName: var string) =
  let isScoped = (blockType != "")
  self.scopes.addLast(isScoped)
  if isScoped:
    # type is always there
    let gotType = self.consumeToken()
    if gotType != blockType:
      self.raiseReadError("expected block of type " & blockType & ", got " & gotType)
    let nextTok = self.consumeToken()
    if nextTok != "{":
      blockName = nextTok
      self.expect({ '{' })

proc consumeBlockEnd(self: TextSerializer) =
  if self.scopes.len() == 0:
    self.raiseError("block end without matching block start")
  let wasScoped = self.scopes.popLast()
  if wasScoped:
    self.expect({ '}' })

proc consumeField(self: TextSerializer): (string, string) =
  self.consumeGarbage()
  let gotName = self.consumeToken(false) # has to be an identifier
  self.consumeGarbage()
  result = (gotName, self.consumeUntil({ ';' }).strip())

proc expectField(self: TextSerializer, fieldName: string): string =
  # try to read from cached fields first, if there are any
  if fieldName in self.curBlock:
    return self.curBlock[fieldName]
  let (gotName, gotValue) = self.consumeField()
  if gotName != fieldName:
    self.raiseReadError("expected field " & fieldName & ", got " & gotName)
  result = gotValue

proc emitField(self: TextSerializer, fieldName: string, fieldValue: string) =
  self.writeIndentedLn(fieldName & " " & fieldValue & ";")

proc emitBlockStart(self: TextSerializer, blockType: string = "", blockName: string = "") =
  let isScoped = (blockType != "")
  self.scopes.addLast(isScoped)
  if isScoped:
    # actual block, open it up and indent it
    self.writeIndented(blockType, " ")
    if blockName != "":
      self.write(blockName, " ")
    self.writeLn("{")
    self.indentLevel.inc()

proc emitBlockEnd(self: TextSerializer) =
  if self.scopes.len() == 0:
    self.raiseError("block end without matching block start")
  let wasScoped = self.scopes.popLast()
  if wasScoped:
    # was an actual block, unindent and close
    self.indentLevel.dec()
    self.writeIndentedLn("}")

proc beginMap(self: TextSerializer) =
  if self.isWriting:
    self.emitBlockStart("map")
  else:
    var s = ""
    self.consumeBlockStart("map", s)

proc endMap(self: TextSerializer) =
  if self.isWriting:
    self.emitBlockEnd()
  else:
    self.consumeBlockEnd()

proc checkNextField*(self: TextSerializer, name: string): bool =
  if name in self.curBlock:
    result = true
  else:
    result = (self.peekToken() == name)

proc serialize*[T: SomeInteger](self: TextSerializer, name: string, value: var T) =
  if self.isWriting:
    self.emitField(name, $value)
  else:
    let fieldVal = self.expectField(name)
    var res: BiggestInt
    let chars = parseBiggestInt(fieldVal, res)
    if chars == 0:
      self.raiseReadError("expected integer, got " & fieldVal)
    else:
      value = cast[T](res)

proc serialize*[T: enum](self: TextSerializer, name: string, value: var T) =
  if self.isWriting:
    self.emitField(name, $value)
  else:
    value = parseEnum[T](self.expectField(name))

proc serialize*[T: enum](self: TextSerializer, name: string, value: var set[T]) =
  if self.isWriting:
    self.emitField(name, bitsetToString(value))
  else:
    value = parseBitSet[T](self.expectField(name))

proc serialize*(self: TextSerializer, name: string, value: var DFPoint) =
  if self.isWriting:
    self.emitField(name, $value)
  else:
    value = parseDFPoint(self.expectField(name))

proc serialize*(self: TextSerializer, name: string, value: var DFSize) =
  if self.isWriting:
    self.emitField(name, $value)
  else:
    value = parseDFSize(self.expectField(name))

proc serialize*(self: TextSerializer, name: string, value: var DFColor) =
  if self.isWriting:
    self.emitField(name, $value)
  else:
    value = parseDFColor(self.expectField(name))

# for actual strings
proc serialize*(self: TextSerializer, name: string, value: var string, maxSize: int) =
  if self.isWriting:
    self.emitField(name, value.quote()) # .substr(0, maxSize - 1).quote())
  else:
    value = self.expectField(name).unquote() # .substr(0, maxSize - 1)

# for identifiers and shit
proc serialize*(self: TextSerializer, name: string, value: var string) =
  if self.isWriting:
    self.emitField(name, value)
  else:
    value = self.expectField(name)

proc serialize*(self: TextSerializer, name: string, value: var bool) =
  if self.isWriting:
    self.emitField(name, $value)
  else:
    value = parseDFBool(self.expectField(name))

proc beginBlockType*(self: TextSerializer, blockName: string, blockId: int): bool =
  if self.isWriting:
    if blockName != "" and blockId >= 0:
      self.writeLn("")
      self.writeIndentedLn("// ", blockName, " section")
    result = true
  else:
    result = (blockName == "") or (self.peekToken() == blockName)

proc inBlockType*(self: TextSerializer, blockName: string, blockId: int): bool =
  if self.isWriting:
    result = true
  else:
    result = (blockName == "") or (self.peekToken() == blockName)

proc endBlockType*(self: TextSerializer) =
  if self.isWriting and self.curBlockType != "" and self.curBlockType != "triggerdata":
    self.writeLn("")
  self.curBlockType = ""

proc beginBlockItem*(self: TextSerializer, blockName: string, blockId: int, value: Serializable): bool =
  if self.isWriting:
    if blockName != "" and blockId >= 0:
      self.writeLn("")
    self.emitBlockStart(blockName, value.name)
  else:
    self.consumeBlockStart(blockName, value.name)
    # if this is a scoped block, pre-read the fields because they can be in random order in there
    self.curBlock.clear()
    if blockName != "":
      var ch = self.peekToken()
      while ch != "}" and ch != "triggerdata": # triggerdata is always at the end
        let (fieldName, fieldVal) = self.consumeField()
        self.curBlock[fieldName] = fieldVal
        ch = self.peekToken()
  result = true

proc endBlockItem*(self: TextSerializer) =
  if self.isWriting:
    self.emitBlockEnd()
  else:
    self.consumeBlockEnd()
    self.curBlock.clear()

proc finalize*(self: TextSerializer) =
  if self.isWriting:
    self.emitBlockEnd()
  else:
    self.consumeBlockEnd()
  self.isValid = false
