import
  std/streams, std/strutils,
  common, ../utils

type
  BinError* = object of SerializationException
  BinReadError* = object of BinError
  BinWriteError* = object of BinError
  BinSerializer* = ref object of Serializer
    stream: Stream
    blockStart: int
    blockEnd: int
    blockTriggerData: int

const
  ## size of the trigger data block in bytes
  triggerDataSize* = 128
  ## map header ident
  fileMagic: string = "MAP\x01"

## compute size of the binary representation of an arbitrary object with serializable fields
proc binarySize*[T: ref object](self: T): int =
  result = 0
  for origFieldName, fieldSym in fieldPairs(self[]):
    if not (fieldSym.hasCustomPragma(serializeNever) or fieldSym.hasCustomPragma(serializeTextOnly)):
      when fieldSym is string:
        when fieldSym.hasCustomPragma(maxSize):
          result += fieldSym.getCustomPragmaVal(maxSize) # strings are zero padded
        else:
          discard # string of unknown length, fuck it
      elif fieldSym is Serializable:
        when fieldSym.hasCustomPragma(triggerData):
          result += triggerDataSize # trigger data is zero padded
        else:
          result += fieldSym.idSize() # this is a reference
      else:
        result += sizeof(fieldSym) # primitive type

proc isBinaryFormat*(stream: Stream): bool =
  (stream.peekStr(fileMagic.len()) == fileMagic)

proc raiseReadError*(self: BinSerializer, msg: string) =
  self.isValid = false
  raise newException(BinReadError, "error: $1 (byte $2): $3" % [self.name, $self.stream.getPosition(), msg])

proc raiseWriteError*(self: BinSerializer, msg: string) =
  self.isValid = false
  raise newException(BinWriteError, "error: $1 (byte $2): $3" % [self.name, $self.stream.getPosition(), msg])

proc raiseError*(self: BinSerializer, msg: string) =
  if self.isWriting:
    self.raiseWriteError(msg)
  else:
    self.raiseReadError(msg)

proc serializeMagic(self: BinSerializer) =
  if self.isWriting:
    self.stream.write(fileMagic)
  else:
    let test = self.stream.readStr(fileMagic.len)
    if test != fileMagic:
      self.raiseReadError("invalid map file magic")

proc newBinSerializer*(name: string, s: Stream, writing: bool): BinSerializer =
  result = new(BinSerializer)
  result.name = name
  result.stream = s
  result.isValid = true
  result.isWriting = writing
  result.blockStart = -1
  result.blockEnd = -1
  result.blockTriggerData = -1
  result.serializeMagic()

proc readWrite*[T](self: BinSerializer, value: var T) =
  let pos = self.stream.getPosition()
  if self.isWriting:
    self.stream.write(value)
  else:
    self.stream.read(value)

proc checkNextField*(self: BinSerializer, name: string): bool = true # all fields are mandatory in our binary format

proc serialize*[T: SomeInteger](self: BinSerializer, name: string, value: var T) =
  self.readWrite(value)

proc serialize*[T: enum](self: BinSerializer, name: string, value: var T) =
  self.readWrite(value)

proc serialize*[T: enum](self: BinSerializer, name: string, value: var set[T]) =
  self.readWrite(value)

proc serialize*[T: DFPoint](self: BinSerializer, name: string, value: var T) =
  var tmp: int32
  if self.isWriting:
    for x in value.mcomponents():
      tmp = x.int32
      self.readWrite(tmp)
  else:
    for x in value.mcomponents():
      self.readWrite(tmp)
      x = tmp.int

proc serialize*[T: DFSize](self: BinSerializer, name: string, value: var T) =
  var tmp: uint16
  if self.isWriting:
    for x in value.mcomponents():
      tmp = x.uint16
      self.readWrite(tmp)
  else:
    for x in value.mcomponents():
      self.readWrite(tmp)
      x = tmp.int

proc serialize*(self: BinSerializer, name: string, value: var DFColor) =
  for x in value.mcomponents():
    self.readWrite(x)

proc zeroPad*(self: BinSerializer, count: int) =
  if self.isWriting:
    let zero = 0.uint8
    for i in 1 .. count:
      self.stream.write(zero)
  else:
    for i in 1 .. count:
      discard self.stream.readUInt8()

proc serialize*(self: BinSerializer, name: string, value: var string, maxSize: int) =
  if self.isWriting:
    if value.len() >= maxSize:
      self.stream.write(value.substr(0, maxSize - 1))
    else:
      self.stream.write(value)
      self.zeroPad(maxSize - value.len())
  else:
    value = self.stream.readStr(maxSize).strip(false, true, {'\0'})

proc serialize*(self: BinSerializer, name: string, value: var bool) =
  self.readWrite(value) # bools are actually read and written as 1 byte like in DF

proc writeBlockHeader(self: BinSerializer, blockId: int, blockSize: int) =
  self.stream.write(blockId.uint8)
  self.stream.write(0.uint32) # reserved
  self.stream.write(blockSize.uint32)

proc readBlockHeader(self: BinSerializer, blockId: int): int =
  # if this isn't the expected block then skip the entire thing
  let peekId = self.stream.peekUInt8()
  if peekId != blockId.uint8:
    return -1
  discard self.stream.readUInt8()
  discard self.stream.readUInt32()
  return self.stream.readUInt32().int

proc beginBlockType*(self: BinSerializer, blockName: string, blockId: int): bool =
  if blockId >= 0:
    if self.isWriting:
      # only write block header for blocks that have one
      self.writeBlockHeader(blockId, 0) # dummy length for now, we'll rewrite it later
      self.blockStart = self.stream.getPosition()
    else:
      # if this isn't the expected block or this is a weird empty block then skip the entire thing
      let blkSize = self.readBlockHeader(blockId)
      if blkSize <= 0: return false
      self.blockStart = self.stream.getPosition()
      self.blockEnd = self.blockStart + blkSize
  else:
    # remember that we're in a sub block
    self.blockTriggerData = self.stream.getPosition()
  result = true

proc inBlockType*(self: BinSerializer, blockName: string, blockId: int): bool =
  (self.blockStart >= 0) and (self.stream.getPosition() < self.blockEnd)

proc endBlockType*(self: BinSerializer) =
  if self.blockTriggerData >= 0:
    # triggerdata is the only block that can be inside another block
    # pad it to 128 if needed
    let pos = self.stream.getPosition()
    let posEnd = self.blockTriggerData + triggerDataSize
    if pos < posEnd: self.zeroPad(posEnd - pos)
    self.blockTriggerData = -1
  elif self.blockStart >= 4:
    if self.isWriting:
      # rewrite the size
      let blkEnd = self.stream.getPosition()
      let blkSize = (blkEnd - self.blockStart).uint32
      self.stream.setPosition(self.blockStart - 4)
      self.stream.write(blkSize)
      self.stream.setPosition(blkEnd)
    self.blockStart = -1 # in binary format blocks can't be inside each other, except triggerdata
    self.blockEnd = -1
  else:
    self.raiseError("block end without block start")

proc beginBlockItem*(self: BinSerializer, blockName: string, blockId: int, value: Serializable): bool =
  true

proc endBlockItem*(self: BinSerializer) =
  discard

proc finalize*(self: BinSerializer) =
  # write empty end block
  if self.beginBlockType("", 0):
    self.endBlockType()
  self.isValid = false
