## don't serialize this field at all
template serializeNever*() {.pragma.}

## don't serialize this field when using binary serializers
template serializeTextOnly*() {.pragma.}

## in text formats, give this field this name
template serializedFieldName*(name: string) {.pragma.}

## in binary formats, make this field into a block with this ID
template serializedBlockId*(id: int) {.pragma.}

## in text formats, make this field into a block with this type name
template serializedBlockName*(name: string) {.pragma.}

## max length for strings
template maxSize*(sz: int) {.pragma.}

## this object is triggerData and should be padded
template triggerData*() {.pragma.}

## this string denotes a resource path
template resPath*() {.pragma.}

## write this field even if it has the default value
template writeDefault*() {.pragma.}

## override size for binary fields, there are some structs that aren't properly packed
template overrideSize*(skip: int) {.pragma.}

## specify default value
template defaultValue*(val: typed) {.pragma.}

## this value shouldn't be editable in the GUI
template nonEditable*() {.pragma.}

## this value shouldn't show up in the GUI by default
template invisible*() {.pragma.}

type
  ## common exception type
  SerializationException* = object of IOError
  ## root type for all serializable objects (not really required, but for convenience)
  Serializable* = ref object of RootObj
    name* {.serializeNever, nonEditable, invisible.}: string
    placeholder* {.serializeNever, nonEditable, invisible.}: bool
  ## root type for all serializers
  Serializer* = ref object of RootObj
    name*: string
    isValid*: bool
    isWriting*: bool

## default init proc for serializables
proc init*(self: Serializable) =
  discard
