import
  std/streams, std/tables, std/strutils,
  zippy, zippy/ziparchives,
  ../utils,
  common

type
  FsZipArchive* = ref object of FsArchive

proc newFsZipArchive*(name, path: string): FsZipArchive =
  result = new(FsZipArchive)
  result.name = name
  result.path = path
  result.files = initTable[string, FsFileEntry]()

# while zippy is cool and all, the ZipArchive.open() proc loads *the entire fucking archive* into memory at once
# and the header reading and other bullshit is not separated out of it, so we roll our own procs based on zippy's

const
  lfhMagic = 0x04034b50'u32
  cdhMagic = 0x02014b50'u32
  eocdMagic = 0x06054b50'u32

proc checkIfZip*(stream: Stream): bool =
  let sig = stream.peekUInt32()
  result = (sig == lfhMagic or sig == eocdMagic)

proc readLocalFileHeader(self: FsZipArchive) =
  let
    minVersionToExtract = self.stream.readUInt16()
    generalPurposeFlag = self.stream.readUInt16()
    compressionMethod = self.stream.readUInt16()
    lastModifiedTime = self.stream.readUInt16()
    lastModifiedDate = self.stream.readUInt16()
    uncompressedCrc32 = self.stream.readUInt32()
    compressedSize = self.stream.readUInt32().int
    uncompressedSize = self.stream.readUInt32().int
    fileNameLen = self.stream.readUInt16().int
    extraFieldLen = self.stream.readUInt16().int

  if (generalPurposeFlag and 0b100) != 0:
    raise newException(FsReadError, "unsupported ZIP file: " & self.name)

  if (generalPurposeFlag and 0b1000) != 0:
    raise newException(FsReadError, "unsupported ZIP64 file: " & self.name)

  if not (compressionMethod in [0.uint16, 8.uint16]):
    raise newException(FsReadError, "unsupported ZIP compression method " & $compressionMethod & " in " & self.name)

  let fileName = self.stream.readStr(fileNameLen).normalizeDFPath()
  self.stream.skip(extraFieldLen)

  var f = self.newFsFileEntry(fileName)
  f.compressed = (compressionMethod != 0)
  f.dataOfs = self.stream.getPosition()
  f.dataSize = compressedSize
  f.uncompressedSize = uncompressedSize
  self.files[fileName.toUpperAscii()] = f

  self.stream.skip(compressedSize) # skip the actual data

proc readCentralDirectory(self: FsZipArchive) =
  let
    versionMadeBy = self.stream.readUInt16()
    minVersionToExtract = self.stream.readUInt16()
    generalPurposeFlag = self.stream.readUInt16()
    compressionMethod = self.stream.readUInt16()
    lastModifiedTime = self.stream.readUInt16()
    lastModifiedDate = self.stream.readUInt16()
    uncompressedCrc32 = self.stream.readUInt32()
    compressedSize = self.stream.readUInt32().int
    uncompressedSize = self.stream.readUInt32().int
    fileNameLen = self.stream.readUInt16().int
    extraFieldLen = self.stream.readUInt16().int
    fileCommentLen = self.stream.readUInt16().int
    diskNumber = self.stream.readUInt16().int
    internalFileAttr = self.stream.readUInt16()
    externalFileAttr = self.stream.readUInt32()
    relativeOffsetOfLocalFileHeader = self.stream.readUInt32()
  # we don't actually need most of this shit right now, we're mostly after LFHs
  let fileName = self.stream.readStr(fileNameLen)
  self.stream.skip(extraFieldLen + fileCommentLen)
  # nuke directories out of the file list
  if (externalFileAttr and 0x10) == 0x10:
    self.files.del(fileName.toUpperAscii())

proc readEndOfCentralDirectory(self: FsZipArchive) =
  let
    diskNumber = self.stream.readUInt16()
    startDisk = self.stream.readUInt16()
    numRecsOnDisk = self.stream.readUInt16()
    numCentralDirRecs = self.stream.readUInt16()
    centralDirSize = self.stream.readUInt32()
    centralDirOfs = self.stream.readUInt32()
    commentLen = self.stream.readUInt16().int
  # skip comment
  self.stream.skip(commentLen)

proc readMetadata(self: FsZipArchive) =
  while not self.stream.atEnd():
    let sig = self.stream.readUInt32()
    case sig
    of lfhMagic: # local file header
      self.readLocalFileHeader()
    of cdhMagic: # central directory header
      self.readCentralDirectory()
    of eocdMagic: # end of central directory record
      self.readEndOfCentralDirectory()
      return
    else:
      raise newException(FsReadError, "unknown ZIP block " & $sig & " in " & self.name)
  raise newException(FsReadError, "invalid ZIP file: " & self.name)

method open*(self: FsZipArchive) =
  if self.existing:
    self.readMetadata()

method loadFile*(self: FsZipArchive, f: FsFileEntry) =
  f.data = ""
  self.stream.setPosition(f.dataOfs)
  let compressed = self.stream.readStr(f.dataSize)
  if f.compressed:
    f.data = uncompress(compressed, dfDeflate)
  else:
    f.data = compressed
  f.uncompressedSize = f.data.len()

method save*(self: FsZipArchive) =
  # here we can actually use zippy's zip archive interface cause we have shit loaded anyway
  var zip = ZipArchive()
  for f in self.files.values():
    zip.contents[f.path] = ArchiveEntry(contents: f.data)
  zip.writeZipArchive(self.path)
