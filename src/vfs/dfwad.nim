import
  std/tables, std/streams, std/os, std/strutils,
  zippy,
  ../utils,
  common

type
  FsWadArchive* = ref object of FsArchive
    sections: OrderedTable[string, seq[FsFileEntry]]

  ResRecord {.packed.} = object
    name*: array[16, char]
    offset*: uint32 # from start of the data section (2 + 6 + sizeof(ResRecord) * numRecords)
    length*: uint32 # when this is 0, this is a section delimeter

const
  wadMagic = "DFWAD\x01"

proc checkIfWad*(stream: Stream): bool =
  result = (stream.peekStr(wadMagic.len()) == wadMagic)

proc newFsWadArchive*(name, path: string): FsWadArchive =
  result = new(FsWadArchive)
  result.name = name
  result.path = path
  result.files = initTable[string, FsFileEntry]()
  result.sections = initOrderedTable[string, seq[FsFileEntry]]()
  result.sections[""] = @[] # root section always exists and is always first

proc readMetadata(self: FsWadArchive) =
  let magic = self.stream.readStr(wadMagic.len())
  if magic != wadMagic:
    raise newException(FsFormatError, "invalid DFWAD magic")
  let numRec = self.stream.readUInt16()
  var curSection = ""
  var curSectionUpper = ""
  for i in 0..<numRec.int:
    var rec: ResRecord
    self.stream.read(rec)
    if rec.length == 0:
      curSection = rec.name.toString()
      curSectionUpper = curSection.toUpperAscii()
      self.sections[curSection] = @[]
    else:
      var prefix = curSection
      prefix.addSep("/")
      var file = newFsFileEntry(self, prefix & rec.name.toString())
      file.dataOfs = rec.offset.int
      file.dataSize = rec.length.int
      file.compressed = true
      self.files[file.path.toUpperAscii()] = file
      self.sections[curSectionUpper].add(file)

method open*(self: FsWadArchive) =
  if self.existing:
    self.readMetadata()

method loadFile*(self: FsWadArchive, f: FsFileEntry) =
  f.data = ""
  self.stream.setPosition(f.dataOfs)
  let compressed = self.stream.readStr(f.dataSize)
  f.data = uncompress(compressed, dfZlib)
  f.uncompressedSize = f.data.len()

proc updateSections(self: FsWadArchive) =
  for f in self.files.mvalues():
    var (section, fname) = f.path.splitPath()
    section = section.toUpperAscii()
    var doAdd = false
    if section notin self.sections:
      self.sections[section] = @[]
      doAdd = true
    if doAdd or f notin self.sections[section]:
      self.sections[section].add(f)

method save*(self: FsWadArchive) =
  # write DFWAD header
  let numRecords = self.files.len() + self.sections.len() - 1 # root section doesn't have a header
  self.stream.write(wadMagic)
  self.stream.write(numRecords.uint16)

  # create resource info records
  var resRecs: seq[ResRecord] = newSeq[ResRecord](numRecords)

  # remember where the info records start
  let hdrOfs = self.stream.getPosition()

  # write empty info records
  assert(sizeof(resRecs) == sizeof(ResRecord) * resRecs.len())
  self.stream.write(resRecs)

  # update sections list if required
  self.updateSections()

  var idx = 0
  for secName, secFiles in self.sections:
    if secName != "":
      # insert a section header
      secName.toArray(resRecs[idx].name)
      resRecs[idx].length = 0'u32
      resRecs[idx].offset = 0'u32
      idx.inc()
    for f in secFiles:
      # compress the data and write it
      let compressed = compress(f.data, DefaultCompression, dfZlib)
      f.dataSize = compressed.len()
      self.stream.write(compressed)
      # fix up the record
      f.path.extractFilename().toArray(resRecs[idx].name)
      resRecs[idx].length = f.dataSize.uint32
      resRecs[idx].offset = self.stream.getPosition().uint32
      idx.inc()

  # rewrite the info records
  self.stream.setPosition(hdrOfs)
  assert(sizeof(resRecs) == sizeof(ResRecord) * resRecs.len())
  self.stream.write(resRecs)

method close*(self: FsWadArchive) =
  procCall close(FsArchive(self)) # call base method
  self.sections.clear()
  self.sections[""] = @[]
