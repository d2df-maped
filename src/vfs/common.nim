import
  std/streams, std/tables, std/os, std/strutils,
  ../utils

type
  FsFileEntry* = ref object of RootObj
    parent*: FsArchive
    path*: string
    data*: string # if this is "". file is yet to be read
    dataOfs*: int # if this is -1, file is yet to be written
    dataSize*: int
    compressed*: bool
    uncompressedSize*: int # if this is -1, length is unknown

  FsArchive* = ref object of RootObj
    name*: string
    path*: string
    files*: Table[string, FsFileEntry]
    stream*: Stream
    writeable*: bool
    existing*: bool
    changed*: bool

  FsStreamObj* = object of StringStreamObj
    entry*: FsFileEntry
    writing*: bool
  FsStream* = ref FsStreamObj

  FsFormatError* = object of IOError
  FsReadError* = object of IOError
  FsWriteError* = object of IOError

method open*(self: FsArchive) {.base.} =
  raiseBaseMethodCall("open")

method save*(self: FsArchive) {.base.} =
  raiseBaseMethodCall("save")

method loadFile*(self: FsArchive, f: FsFileEntry) {.base.} =
  raiseBaseMethodCall("loadFile")

method close*(self: FsArchive) {.base.} =
  if self.writeable and self.changed:
    # read the contents of all already existing files
    for f in self.files.mvalues():
      if f.data == "":
        self.loadFile(f)
    # if operating on a file, make a backup and truncate it
    if self.path != "":
      self.stream.close()
      try:
        copyFile(self.path, self.path & ".bak")
      except OSError:
        discard
      self.stream = newFileStream(self.path, fmWrite)
    # write the new file
    self.save()
    self.changed = false
  self.files.clear()
  if self.path != "" and self.stream != nil:
    self.stream.close()
  self.stream = nil

proc newFsFileEntry*(archive: FsArchive, path: string): FsFileEntry =
  result = new(FsFileEntry)
  result.parent = archive
  result.path = path
  result.uncompressedSize = -1
  result.dataOfs = -1
  result.data = ""

proc fsClose(s: Stream) =
  var f = FsStream(s)
  var ss = StringStream(s)
  if f.writing:
    f.entry.data = ss.data
    f.entry.dataOfs = -1 # need rewrite
    if f.entry.compressed:
      f.entry.dataSize = -1 # need compression
    else:
      f.entry.dataSize = ss.data.len()
    f.entry.uncompressedSize = ss.data.len()
    f.entry.parent.files[f.entry.path.toUpperAscii()] = f.entry
  else:
    f.entry.data = "" # don't leave that shit cached
  ss.data = ""

proc newFsStream*(f: FsFileEntry, write: bool): FsStream =
  let data = f.data
  result = new(FsStream)
  result.writing = write
  result.entry = f
  # these are not really made to inherit from them
  var ss = newStringStream(data)
  result.data = ss.data
  result.closeImpl = fsClose # our own proc
  result.atEndImpl = ss.atEndImpl
  result.setPositionImpl = ss.setPositionImpl
  result.getPositionImpl = ss.getPositionImpl
  result.readDataStrImpl = ss.readDataStrImpl
  result.readLineImpl = ss.readLineImpl
  result.readDataImpl = ss.readDataImpl
  result.peekDataImpl = ss.peekDataImpl
  result.writeDataImpl = ss.writeDataImpl
  result.flushImpl = ss.flushImpl

proc findFile*(self: FsArchive, localPath: string): FsFileEntry =
  let upperPath = localPath.toUpperAscii()
  if upperPath in self.files:
    return self.files[upperPath]
  result = nil

proc loadFile*(self: FsArchive, localPath: string) =
  var f = self.findFile(localPath)
  if f == nil:
    raise newException(FsReadError, "file " & localPath & " does not exist in " & self.name)
  self.loadFile(f)

proc writeFile*(self: FsArchive, localPath: string, compressed: bool = true): Stream =
  if not self.writeable:
    raise newException(FsWriteError, self.name & " is not writeable")
  var f = self.findFile(localPath)
  if f == nil:
    f = self.newFsFileEntry(localPath.toUpperAscii())
    f.compressed = compressed
  else:
    self.loadFile(f)
  result = newFsStream(f, true)

proc readFile*(self: FsArchive, localPath: string): Stream =
  var f = self.findFile(localPath)
  if f == nil:
    return nil
  self.loadFile(f)
  result = newFsStream(f, false)
